#!/usr/bin/env bash
# !/bin/bas

INSTALLATION_PATH="/opt/EK_Crawler/ControllerServer/"

if [ -d "$INSTALLATION_PATH" ]; then

    read -p "Previous installation detected, do you want to delete previous files? (y/n) " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        rm -rd "$INSTALLATION_PATH"
    else
        exit 1
    fi

fi

curr_dir = pwd
mkdir -p $INSTALLATION_PATH
cd "$INSTALLATION_PATH"

mkdir ssl
mkdir JS_zoo
mkdir JS_zoo/malicious
mkdir JS_zoo/pending_analyze
mkdir /var/lib/EK_Crawler
mkdir /var/lib/EK_Crawler/Download_Scripts/
chmod 777 /var/lib/EK_Crawler/Download_Scripts/
cd ssl

if which openssl >/dev/null; then
    echo "Openssl previously installed..."
else
    apt-get install openssl
fi

echo "Generating selfsigned ssl certificate (error... esta autofirmado???)"
# openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365
cp "$curr_dir/cert.pem" .

if which psql >/dev/null; then
    echo "Postgresql previously installed..."
else
    apt-get install postgresql postgresql-contrib
fi

echo "Creating database"

exec sudo -u postgres /bin/bash - << eof

if psql EK_Crawler -c '\q' 2>&1; then
	read -p "Another EK_Crawler database detected, do you want to delete it? (y/n) " -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		dropdb EK_Crawler
	else
		exit 1
	fi
	
fi

createdb EK_Crawler
psql -d EK_Crawler

ALTER USER postgres WITH PASSWORD 'ControllerServerAutomaticPassw0rd';

CREATE TABLE Indexer_CVE (cve VARCHAR (17) PRIMARY KEY, product VARCHAR (30) NOT NULL, vendor VARCHAR (30), version VARCHAR(15));
CREATE TABLE Indexer_Keywords (id SERIAL PRIMARY KEY, date TIMESTAMP NOT NULL DEFAULT current_timestamp, keyword VARCHAR (30) NOT NULL, enabled BOOLEAN DEFAULT true );
CREATE TABLE Indexer_Hosts (ip VARCHAR (2048) PRIMARY KEY, id_kw INTEGER REFERENCES Indexer_Keywords (id), cve VARCHAR (17) REFERENCES Indexer_CVE (cve), enabled BOOLEAN DEFAULT true, last_send TIMESTAMP);

CREATE TABLE Crawler_user_agents (id SERIAL PRIMARY KEY, browser VARCHAR (30), browser_version VARCHAR (15), so VARCHAR (30), so_version VARCHAR (15), user_agent VARCHAR (4096) NOT NULL, enabled BOOLEAN NOT NULL DEFAULT true);
CREATE TABLE Crawler_vpn (country VARCHAR (2) PRIMARY KEY, config_file VARCHAR (4096) NOT NULL, enabled BOOLEAN NOT NULL DEFAULT true);
CREATE TABLE Crawler_hosts (id SERIAL PRIMARY KEpgY, content_hash VARCHAR (32) NOT NULL, ip VARCHAR (2048) NOT NULL, path VARCHAR (2048) NOT NULL, referrer VARCHAR (2048) NOT NULL, last_check TIMESTAMP NOT NULL DEFAULT current_timestamp, id_ua INTEGER NOT NULL REFERENCES Crawler_user_agents (id), country_vpn VARCHAR (2) NOT NULL REFERENCES Crawler_vpn (country), content TEXT NOT NULL, last_send TIMESTAMP, count INTEGER NOT NULL DEFAULT 0);

CREATE TABLE Analyzer_plugins (name VARCHAR (15) PRIMARY KEY, enabled BOOLEAN NOT NULL DEFAULT true);
CREATE TABLE Analyzer_reports (id SERIAL PRIMARY KEY, hash VARCHAR (32) NOT NULL, plugin VARCHAR (20) NOT NULL, date TIMESTAMP NOT NULL DEFAULT current_timestamp, report BOOLEAN NOT NULL, description TEXT); 

CREATE TABLE System_keys (key VARCHAR(16) PRIMARY KEY, agent VARCHAR(8) NOT NULL, ip VARCHAR(12) NOT NULL, date TIMESTAMP NOT NULL DEFAULT current_timestamp, state BOOLEAN NOT NULL DEFAULT true);


INSERT INTO Crawler_vpn VALUES ('ES', '', TRUE);
INSERT INTO Crawler_user_agents VALUES (0, 'IE', '8.0', 'Windows', '7', 'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; GTB7.4; InfoPath.2; SV1; .NET CLR 3.3.69573; WOW64; en-US)', TRUE);
INSERT INTO Indexer_hosts VALUES ('8.8.8.8', NULL, NULL, TRUE, NULL);


CREATE OR REPLACE FUNCTION ExportToFile() RETURNS trigger AS $$
BEGIN
	EXECUTE format('COPY(SELECT ch.content FROM Crawler_hosts ch WHERE ch.id = %L) TO ''/var/lib/EK_Crawler/Download_Scripts/%s'' (format text)', new.id, cast(new.id as text));
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER CrawlerContentToFile AFTER INSERT OR UPDATE ON Crawler_Hosts
    FOR EACH ROW EXECUTE PROCEDURE ExportToFile();

\q

exit
eof
