import requests
from urllib.parse import urlparse
from .. import Log


class HTTPHandler:

    def __init__(self):

        Log.business_layer("INFO", "Creating HTTPHandler class")

        self.CURRENT_HOST = ""
        self.CURRENT_SCHEMA = ""
        self.CURRENT_USER_AGENT = ""

        self.VPN = ""

    def download_html(self, url_queue_element):

        Log.business_layer("INFO", "Downloading HTML content")

        if url_queue_element.VPN != self.VPN:
            Log.business_layer("INFO", "New VPN configuration requested")
            self.VPN = url_queue_element.VPN
            #  Configurar la vpn correcta

        self.CURRENT_HOST = url_queue_element.HOST
        self.CURRENT_SCHEMA = url_queue_element.SCHEMA
        self.CURRENT_USER_AGENT = url_queue_element.USER_AGENT

        return self.download(url_queue_element.USER_AGENT, url_queue_element.REFERRER, url_queue_element.SCHEMA, url_queue_element.HOST, url_queue_element.PATH)

    def download_js(self, host):

        Log.business_layer("INFO", "Downloading JS content")

        if host[:3].upper() != "HTT":
            parsed_url = urlparse(host)
        elif host[:2].upper() != "//":
            parsed_url = urlparse(host)
        else:
            parsed_url = urlparse("//" + host)

        host_ = parsed_url.netloc
        schema_ = parsed_url.scheme
        path_ = parsed_url.path

        if host_ == "":
            host_ = self.CURRENT_HOST
        if schema_ == "":
            schema_ = self.CURRENT_SCHEMA

        return self.download(self.CURRENT_USER_AGENT, self.CURRENT_HOST, schema_, host_, path_)

    @staticmethod
    def download(user_agent, referrer, schema, host, path):
        headers = {'User-Agent': user_agent, 'Referer': referrer}

        attempts = 3  # Attempts of download the same content
        success = False

        while not success:
            try:
                response = requests.get(schema + host + path, verify=False, timeout=10, headers=headers)
                success = True
                Log.business_layer("INFO", "Downloaded, response code received: " + str(response.status_code))
                if 199 < response.status_code < 300:  # 2xx response code is OK in HTTP protocol
                    return response.content.decode("utf-8").replace("__", "[_][_]")
                elif 399 < response.status_code < 500:  # 4xx Client error
                    attempts = 0
            except Exception as e:  # Critical errors during the connection process requests.exceptions.RequestException

                Log.business_layer("WARNING", "Download failed, remaining attempts: " + str(attempts))

                if attempts > 0:
                    attempts -= 1
                    success = False
                else:
                    Log.business_layer("WARNING", "Download failed, no attempts remaining: " + str(e))
                    break

        return "Error"
