from html.parser import HTMLParser
from .. import Log


class HtmlHandler (HTMLParser):
    """Class to implement custom HTML parser"""

    def __init__(self, http_handler):
        """Custom constructor of the parser"""

        Log.business_layer("INFO", "Creating HtmlHandler class")

        HTMLParser.__init__(self)  # Super constructor
        self.HTTP_HANDLER = http_handler

        self.IS_SCRIPT = False
        self.CURRENT_CONTENT = ""
        self.SCRIPT_CONTENT = ""

    def handle_starttag(self, tag, attrs):
        """Function called each time a new tag is found <tag>"""

        src_content = ""
        self.CURRENT_CONTENT = ""

        if tag == "script":  # Script begin, start recording content data

            Log.business_layer("INFO", "Found <script> start")

            for name, value in attrs:  # Iterate over script attributes to fund the src url

                if name == "src":  # If src is found, the content will be downloaded
                    Log.business_layer("INFO", "Found <script> external src")
                    src_content = self.HTTP_HANDLER.download_js(value)

            self.IS_SCRIPT = True
            self.CURRENT_CONTENT = src_content

    def handle_endtag(self, tag):
        """Function called each time a close tag is found </tag>"""

        if tag == "script":  # Script ended, stop recording content data

            Log.business_layer("INFO", "Found <script> end")

            self.IS_SCRIPT = False
            self.SCRIPT_CONTENT += "\n\n//---------------------START SCRIPT TAG CONTENT---------------------\n\n" + self.CURRENT_CONTENT + "\n\n//---------------------END SCRIPT TAG CONTENT---------------------\n\n"

    def handle_data(self, data):
        """Function called when a content between tags is found <tag>That content</tag>"""

        if self.IS_SCRIPT:  # Only record if script content

            Log.business_layer("INFO", "Found <script> content")

            self.CURRENT_CONTENT += data
