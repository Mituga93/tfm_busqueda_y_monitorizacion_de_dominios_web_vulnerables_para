
from ..Communication import ProtocolParser
from ..Communication import ProtocolModels
from ..Communication import SecureSocket
from .. import Log
from . import HtmlHandler
from . import HttpHandler
try:
    import selectors
except ImportError:
    import selectors34 as selectors
import hashlib
import datetime
import time
import re
from urllib.parse import urlparse


class URLQueueElement:

    def __init__(self, host, user_agent, vpn, referrer, internal_count, external_count):

        if host[:3].upper() != "HTT":
            parsed_url = urlparse(host)
        elif host[:2].upper() != "//":
            parsed_url = urlparse(host)
        else:
            parsed_url = urlparse("//" + host)

        host_ = parsed_url.netloc
        path_ = parsed_url.path
        scheme_ = parsed_url.scheme

        if host_ == "":
            host_ = path_
            path_ = ""

        if scheme_ == "":
            scheme_ = "http://"
        else:
            scheme_ += "://"

        Log.business_layer("INFO", "Creating new URLQueueElement class with: (" + str(parsed_url.netloc) + ", " + str(parsed_url.path) + ")")

        self.HOST = host_
        self.PATH = path_
        self.USER_AGENT = user_agent
        self.VPN = vpn
        self.REFERRER = referrer

        self.SCHEMA = scheme_
        self.INTERNAL_COUNT = internal_count
        self.EXTERNAL_COUNT = external_count


class CrawlerAgent:

    def __init__(self, ip, port, key):

        Log.business_layer("INFO", "Creating new CrawlerAgent class with: (" + ip + ", " + str(port) + ", " + key + ")")

        self.KEY = key

        # self.MANAGEMENT_SOCKET = SecureSocket.AgentSecureSocket(ip, port)
        self.WORKER_SOCKETS = []

        self.MANAGEMENT_PROTOCOL_PARSER = ProtocolParser.CrawlerProtocolParser(key)
        self.WORKER_PROTOCOL_PARSER = []

        self.SHOULD_STOP = False
        self.STOPPED = False

        self.URL_QUEUE = []

        self.CURRENT_VPN = ""

        self.HTTP_HANDLER = HttpHandler.HTTPHandler()
        self.HTML_HANDLER = HtmlHandler.HtmlHandler(self.HTTP_HANDLER)

        self.add_worker_socket(SecureSocket.AgentSecureSocket(ip, port))

    def start_management(self):

        Log.business_layer("INFO", "Running management on CrawlerAgent class with: " + self.KEY)

        while True:
            self.management_event_received()

    def start_workers(self):
        
        Log.business_layer("INFO", "Running worker(s) on CrawlerAgent class with: " + self.KEY)

        while True:
            self.worker_event_received(self.WORKER_SOCKETS[0], 0)

    def add_worker_socket(self, socket):
        
        Log.business_layer("INFO", "Adding new worker socket on CrawlerAgent class with: " + self.KEY)

        self.WORKER_SOCKETS.append(socket)

        self.WORKER_PROTOCOL_PARSER.append(ProtocolParser.CrawlerProtocolParser(self.KEY))

        # self.SELECTOR.register(socket, selectors.EVENT_READ, (len(self.WORKER_SOCKETS) - 1, socket))

    def management_event_received(self):

        Log.business_layer("INFO", "Receiving messages by management socket on CrawlerAgent with " + self.KEY)

        socket = self.MANAGEMENT_SOCKET

        msg = socket.rcv_all()

        object_received = self.MANAGEMENT_PROTOCOL_PARSER.parse_rcved_msg(msg)

        if object_received.MSG_TYPE == "HELLO":
            self.hello(socket, -1)
        elif object_received.MSG_TYPE == "RESUME":
            self.resume()
        elif object_received.MSG_TYPE == "STOP":
            self.stop()
        else:
            Log.business_layer("WARNING", "Ignored unidentified message received by management socket on CrawlerAgent with " + self.KEY + " MSG_TYPE: " + object_received.MSG_TYPE)

    def worker_event_received(self, socket, sock_id):

        Log.business_layer("INFO", "Receiving messages by worker socket on CrawlerAgent with " + self.KEY + " and ID: " + str(sock_id))

        msg = socket.rcv_all()

        object_received = self.WORKER_PROTOCOL_PARSER[sock_id].parse_rcved_msg(msg)

        if object_received is None:
            Log.business_layer("WARNING", "Ignored message (inherited) received by worker socket on CrawlerAgent with " + self.KEY + "ID: " + str(sock_id))
        elif object_received.MSG_TYPE == "HELLO":
            self.hello(socket, sock_id)
        elif self.STOPPED:
            Log.business_layer("INFO", "Worker is stopped by server on CrawlerAgent with " + self.KEY + " and ID: " + str(sock_id))
            time.sleep(10)
        elif self.SHOULD_STOP and not self.STOPPED:
            Log.business_layer("INFO", "Worker is stopped by server on CrawlerAgent with " + self.KEY + " and ID: " + str(sock_id))
            self.STOPPED = True
            self.send_stop()
        elif object_received.MSG_TYPE == "GET HOST":
            self.get_host(object_received, socket, sock_id)
        else:
            Log.business_layer("WARNING", "Ignored unidentified message received by worker socket on CrawlerAgent with " + self.KEY + "ID: " + str(sock_id) + " MSG_TYPE: " + object_received.MSG_TYPE)

    def hello(self, socket, sock_id):

        Log.business_layer("INFO", "Handling HELLO on CrawlerAgent with " + self.KEY + " and ID: " + str(sock_id))

        if sock_id < 0:
            socket_type = "MANAGEMENT"
            protocol_parser = self.MANAGEMENT_PROTOCOL_PARSER
        else:
            socket_type = "WORKER"
            protocol_parser = self.WORKER_PROTOCOL_PARSER[sock_id]

        socket.send_all(protocol_parser.construct_to_send_msg(ProtocolModels.HelloAnswer("CRAWLER", self.KEY, socket_type)))

        time.sleep(5)

        self.send_get_host(socket, sock_id)

    def resume(self):

        Log.business_layer("INFO", "Handling RESUME on CrawlerAgent with " + self.KEY)

        self.SHOULD_STOP = False
        self.send_resume()

    def stop(self):

        Log.business_layer("INFO", "Handling STOP on CrawlerAgent with " + self.KEY)

        self.SHOULD_STOP = True

    def send_get_host(self, socket, sock_id):

        socket.send_all(self.WORKER_PROTOCOL_PARSER[sock_id].construct_to_send_msg(ProtocolModels.GetHostMessage("", self.KEY)))

    def get_host(self, object_received, socket, sock_id):

        Log.business_layer("INFO", "Handling GET HOST on CrawlerAgent with " + self.KEY)

        queue_element = URLQueueElement(object_received.HOST, object_received.USER_AGENT, object_received.VPN, "www.google.es", 2, 1)

        self.URL_QUEUE.append(queue_element)
        url_history = ""

        while len(self.URL_QUEUE) > 0:

            Log.business_layer("INFO", "Iteration on GET HOST loop on CrawlerAgent with " + self.KEY + " and queue length: " + str(len(self.URL_QUEUE)))

            current_element = self.URL_QUEUE[0]

            downloaded_content = self.HTTP_HANDLER.download_html(current_element)

            relations = []
            # relations = re.finditer("(ftp:\/\/|www\.|https?:\/\/){1}[a-zA-Z0-9u00a1-\uffff0-]{2,}\.[a-zA-Z0-9u00a1-\uffff0-]{2,}(\S*)", downloaded_content, flags=re.MULTILINE)
            # relations = re.finditer("/(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/igm", downloaded_content, flags=re.MULTILINE)
            relations = re.findall("(https?)?(:\/\/|\/\/|\/)?[w0-9.]{0,5}([a-zA-Z0-9-\/]{2,}\.){2,}[a-zA-Z0-9]{2,}(\/[a-zA-Z0-9-._~!()*%.]{2,})?", downloaded_content, flags=re.MULTILINE)

            relations = list(set(relations))

            for relation in relations:

                queue_element = URLQueueElement(relation.group(0), object_received.USER_AGENT, object_received.VPN, current_element.HOST, current_element.INTERNAL_COUNT - 1, current_element.EXTERNAL_COUNT - 1)

                if (queue_element.HOST == "" or queue_element.HOST == current_element.HOST) and queue_element.PATH != "":
                    queue_element.HOST = current_element.HOST
                    rem_count = queue_element.INTERNAL_COUNT
                    #queue_element.INTERNAL_COUNT -= 1
                    #queue_element.EXTERNAL_COUNT = 0
                else:
                    rem_count = queue_element.EXTERNAL_COUNT
                    #queue_element.EXTERNAL_COUNT -= 1
                    #queue_element.INTERNAL_COUNT = 0
                if rem_count > 0 and (queue_element.HOST + queue_element.PATH) not in url_history:
                    self.URL_QUEUE.append(queue_element)
                    url_history += "|" + queue_element.HOST + queue_element.PATH


            Log.business_layer("INFO", "Size of queue after find references: " + str(len(self.URL_QUEUE)) + " on GET HOST loop on CrawlerAgent with " + self.KEY)

            self.HTML_HANDLER.SCRIPT_CONTENT = "" # sobrecargar el feed para hacer eso podria ser mejor idea...
            self.HTML_HANDLER.feed(str(downloaded_content))

            parsed_content = self.HTML_HANDLER.SCRIPT_CONTENT

            Log.business_layer("INFO", "Detected: " + str(len(parsed_content)) + " characters of <script> on GET HOST loop on CrawlerAgent with " + self.KEY)

            self.send_set_host(current_element, socket, sock_id, parsed_content)
            self.set_host(socket, sock_id)

            self.URL_QUEUE.remove(self.URL_QUEUE[0])
            self.URL_QUEUE = list(set(self.URL_QUEUE))
        self.send_get_host(socket, sock_id)

    def send_set_host(self, url_queue_element, socket, sock_id, content):

        host_s = url_queue_element.HOST.split("/")
        if len(host_s) < 1:
            path = "/"
            host = url_queue_element.HOST
        else:
            path = ""
            host = host_s[0]
            for part in host_s[1:]:
                path += part

        Log.business_layer("INFO", "Sending SET HOST message on CrawlerAgent with " + self.KEY + " with: (" + host + ", " + path + ")")

        msg = self.WORKER_PROTOCOL_PARSER[sock_id].construct_to_send_msg(ProtocolModels.SetHostMessage("", self.KEY, host, url_queue_element.USER_AGENT, url_queue_element.VPN, hashlib.md5(content.encode()).hexdigest(), path, url_queue_element.REFERRER, datetime.datetime.now(), "P", content))

        socket.send_all(msg)

    def send_stop(self):

        Log.business_layer("INFO", "Sending STOP answer on CrawlerAgent with " + self.KEY)

        self.MANAGEMENT_SOCKET.send_all(self.MANAGEMENT_PROTOCOL_PARSER.construct_to_send_msg(ProtocolModels.StopAnswer()))
        self.STOPPED = True

    def send_resume(self):

        Log.business_layer("INFO", "Sending RESUME answer on CrawlerAgent with " + self.KEY)

        self.MANAGEMENT_SOCKET.send_all(self.MANAGEMENT_PROTOCOL_PARSER.construct_to_send_msg(ProtocolModels.ResumeAnswer()))
        self.STOPPED = False

    def set_host(self, socket, sock_id):

        Log.business_layer("INFO", "Expecting SET HOST answer on CrawlerAgent with " + self.KEY)

        msg = socket.rcv_all()

        object_received = self.WORKER_PROTOCOL_PARSER[sock_id].parse_rcved_msg(msg)

        if object_received.MSG_TYPE != "SET HOST":
            Log.business_layer("INFO", "Received an unexpected message while expecting SET HOST answer on CrawlerAgent with " + self.KEY + " MSG_TYPE: " + object_received.MSG_TYPE)


