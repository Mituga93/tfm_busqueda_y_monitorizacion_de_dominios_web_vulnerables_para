from . import ProtocolModels
from .. import Log


class CrawlerProtocolParser:
    def __init__(self, key):

        Log.business_layer("INFO", "Creating CrawlerProtocolParser with: " + key)

        self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                  0,  # 1 ANS GET HOST
                                  0,  # 2 SET HOST
                                  0,  # 3 ANS SET HOST
                                  0,  # 4 STOP
                                  0,  # 5 ANS STOP
                                  0,  # 6 RESUME
                                  0,  # 7 ANS RESUME
                                  1,  # 8 HELLO
                                  0]  # 9 ANS HELLO

        self.KEY = key
        self.AGENT_TYPE = "CRAWLER"

    def parse_rcved_msg(self, msg):

        Log.business_layer("INFO", "Receiving message on CrawlerProtocolParser with: " + self.KEY)

        msg_s = msg.split("__", 15)

        if len(msg_s) > 14 or len(msg_s) == 0:
            Log.comms_layer("WARNING",
                            "Ignored an impossible to parse message on CrawlerProtocolParser class with: " + self.KEY + ") and MSG: " + msg)
        elif msg_s[0] == "HELLO":
            return self.rcved_hello(msg_s)
        elif msg_s[1] == "GET HOST":
            return self.rcved_get_host(msg_s)
        elif msg_s[1] == "SET HOST":
            return self.rcved_set_host(msg_s)
        elif msg_s[0] == "RESUME":
            return self.rcved_resume(msg_s)
        elif msg_s[0] == "STOP":
            return self.rcved_stop(msg_s)
        else:
            Log.comms_layer("WARNING",
                            "Ignored an impossible to identify message on CrawlerProtocolParser class with: " + self.KEY + ") and MSG: " + msg)

        return None

    def rcved_get_host(self, msg_s):

        if self.ALLOWED_MSG_STATE[1] != 1:
            Log.comms_layer("WARNING",
                            "Ignored a non expected GET HOST answer on CrawlerProtocolParser class with: " + self.KEY)
        elif len(msg_s) != ProtocolModels.GetHostAnswer.get_number_param():
            Log.comms_layer("WARNING",
                            "Ignored an invalid GET HOST answer on CrawlerProtocolParser class with: " + self.KEY)
        else:
            self.ALLOWED_MSG_STATE = [1,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      1,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 STOP
                                      0,  # 5 ANS STOP
                                      0,  # 6 RESUME
                                      0,  # 7 ANS RESUME
                                      0,  # 8 HELLO
                                      0]  # 9 ANS HELLO
            return ProtocolModels.GetHostAnswer(msg_s[2], msg_s[3], msg_s[4])

        return None

    def rcved_set_host(self, msg_s):

        if self.ALLOWED_MSG_STATE[3] != 1:
            Log.comms_layer("WARNING",
                            "Ignored a non expected SET HOST message on CrawlerProtocolParser class with: " + self.KEY)
        elif len(msg_s) != ProtocolModels.SetHostAnswer.get_number_param():
            Log.comms_layer("WARNING",
                            "Ignored an invalid SET HOST message on CrawlerProtocolParser class with: " + self.KEY)
        else:
            self.ALLOWED_MSG_STATE = [1,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      1,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 STOP
                                      0,  # 5 ANS STOP
                                      0,  # 6 RESUME
                                      0,  # 7 ANS RESUME
                                      0,  # 8 HELLO
                                      0]  # 9 ANS HELLO
            return ProtocolModels.SetHostAnswer()

        return None

    def rcved_stop(self, msg_s):

        if self.ALLOWED_MSG_STATE[4] != 1:
            Log.comms_layer("WARNING",
                            "Ignored a non expected STOP message on CrawlerProtocolParser class with: " + self.KEY)
        elif len(msg_s) != ProtocolModels.StopMessage.get_number_param():
            Log.comms_layer("WARNING",
                            "Ignored an invalid STOP message on CrawlerProtocolParser class with: " + self.KEY)
        else:
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 STOP
                                      1,  # 5 ANS STOP
                                      1,  # 6 RESUME
                                      0,  # 7 ANS RESUME
                                      0,  # 8 HELLO
                                      0]  # 9 ANS HELLO
            return ProtocolModels.StopMessage(msg_s[1])

        return None

    def rcved_resume(self, msg_s):

        if self.ALLOWED_MSG_STATE[6] != 1:
            Log.comms_layer("WARNING",
                            "Ignored a non expected RESUME message on CrawlerProtocolParser class with: " + self.KEY)
        elif len(msg_s) != ProtocolModels.ResumeMessage.get_number_param():
            Log.comms_layer("WARNING",
                            "Ignored an invalid RESUME message on CrawlerProtocolParser class with: " + self.KEY)
        else:
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 STOP
                                      0,  # 5 ANS STOP
                                      0,  # 6 RESUME
                                      1,  # 7 ANS RESUME
                                      0,  # 8 HELLO
                                      0]  # 9 ANS HELLO
            return ProtocolModels.ResumeMessage()

        return None

    def rcved_hello(self, msg_s):

        if self.ALLOWED_MSG_STATE[8] != 1:
            Log.comms_layer("WARNING",
                            "Ignored a non expected HELLO message on CrawlerProtocolParser class with: " + self.KEY)
        elif len(msg_s) != ProtocolModels.HelloMessage.get_number_param():
            Log.comms_layer("WARNING",
                            "Ignored an invalid HELLO message on CrawlerProtocolParser class with: " + self.KEY)
        else:
            self.set_normal_state()
            return ProtocolModels.HelloMessage()

        return None

    def construct_to_send_msg(self, msg_o):

        Log.comms_layer("INFO",
                        "Constructing protocol message on CrawlerProtocolParser class with: " + self.KEY + " and MSG_TYPE: " + msg_o.MSG_TYPE)

        msg_s = msg_o.get_sorted_param_list()

        if msg_o.MSG_TYPE == "GET HOST":
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      1,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 STOP
                                      0,  # 5 ANS STOP
                                      0,  # 6 RESUME
                                      0,  # 7 ANS RESUME
                                      0,  # 8 HELLO
                                      0]  # 9 ANS HELLO
        elif msg_o.MSG_TYPE == "SET HOST":
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      1,  # 3 ANS SET HOST
                                      0,  # 4 STOP
                                      0,  # 5 ANS STOP
                                      0,  # 6 RESUME
                                      0,  # 7 ANS RESUME
                                      0,  # 8 HELLO
                                      0]  # 9 ANS HELLO
        elif msg_o.MSG_TYPE == "STOP":
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 STOP
                                      0,  # 5 ANS STOP
                                      1,  # 6 RESUME
                                      0,  # 7 ANS RESUME
                                      0,  # 8 HELLO
                                      0]  # 9 ANS HELLO
        elif msg_o.MSG_TYPE == "RESUME":
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      1,  # 4 STOP
                                      0,  # 5 ANS STOP
                                      0,  # 6 RESUME
                                      0,  # 7 ANS RESUME
                                      0,  # 8 HELLO
                                      0]  # 9 ANS HELLO
        elif msg_o.MSG_TYPE == "HELLO":
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 STOP
                                      0,  # 5 ANS STOP
                                      0,  # 6 RESUME
                                      0,  # 7 ANS RESUME
                                      0,  # 8 HELLO
                                      1]  # 9 ANS HELLO

        msg = ""
        for msg_p in msg_s:
            msg += str(msg_p) + "__"

        msg += "\r\n\r\n"

        return msg

    def set_normal_state(self):

        Log.comms_layer("INFO", "Recovering to normal status on CrawlerProtocolParser class with: " + self.KEY)

        self.ALLOWED_MSG_STATE = [1,  # 0 GET HOST
                                  0,  # 1 ANS GET HOST
                                  0,  # 2 SET HOST
                                  0,  # 3 ANS SET HOST
                                  1,  # 4 STOP
                                  0,  # 5 ANS STOP
                                  0,  # 6 RESUME
                                  0,  # 7 ANS RESUME
                                  0,  # 8 HELLO
                                  0]  # 9 ANS HELLO
