
class WorkerProtocolMessage:

    def __init__(self, agent, source, key, msg_type):

        self.AGENT = agent
        self.SOURCE = source
        self.KEY = key
        self.MSG_TYPE = msg_type

    @staticmethod
    def get_number_param():

        return 4  # 3 + EOF (source ignored)

    def get_sorted_param_list(self):

        return [self.AGENT,
                self.KEY,
                self.MSG_TYPE]


class GetHostMessage(WorkerProtocolMessage):

    def __init__(self, source, key):

        WorkerProtocolMessage.__init__(self, "CRAWLER", source, key, "GET HOST")

    @staticmethod
    def get_number_param():

        return WorkerProtocolMessage.get_number_param() + 0


class SetHostMessage(WorkerProtocolMessage):

    def __init__(self, source, key, host, user_agent, vpn, hash_, path, referrer, datetime, status, content):

        WorkerProtocolMessage.__init__(self, "CRAWLER", source, key, "SET HOST")

        self.HOST = host
        self.USER_AGENT = user_agent
        self.VPN = vpn
        self.HASH = hash_
        self.PATH = path
        self.REFERRER = referrer
        self.DATETIME = datetime
        self.STATUS = status
        self.CONTENT = content

    @staticmethod
    def get_number_param():

        return WorkerProtocolMessage.get_number_param() + 9

    def get_sorted_param_list(self):

        return super(SetHostMessage, self).get_sorted_param_list() + [
            self.HOST, self.USER_AGENT, self.VPN, self.HASH, self.PATH,
            self.REFERRER, self.DATETIME, self.STATUS, self.CONTENT]


class GetContentMessage(WorkerProtocolMessage):

    def __init__(self, source, key):

        WorkerProtocolMessage.__init__(self, "ANALYZER", source, key, "GET CONTENT")

    @staticmethod
    def get_number_param():

        return WorkerProtocolMessage.get_number_param() + 0


class SetContentMessage(WorkerProtocolMessage):

    def __init__(self, source, key, hash_, test, datetime, result, desc):

        WorkerProtocolMessage.__init__(self, "ANALYZER", source, key, "SET CONTENT")

        self.HASH = hash_
        self.TEST = test
        self.DATETIME = datetime
        self.RESULT = result
        self.DESC = desc

    @staticmethod
    def get_number_param():

        return WorkerProtocolMessage.get_number_param() + 5

    def get_sorted_param_list(self):

        return super(SetContentMessage, self).get_sorted_param_list() + [self.HASH, self.TEST, self.DATETIME, self.RESULT, self.DESC]


class WorkerProtocolAnswer:

    def __init__(self, msg_type):

        self.MSG_TYPE = msg_type

    @staticmethod
    def get_number_param():

        return 3  # 1 + EOF + ANS

    def get_sorted_param_list(self):

        return ["ANS", self.MSG_TYPE]


class GetHostAnswer(WorkerProtocolAnswer):

    def __init__(self, host, user_agent, vpn):

        WorkerProtocolAnswer.__init__(self, "GET HOST")
        self.HOST = host
        self.USER_AGENT = user_agent
        self.VPN = vpn

    @staticmethod
    def get_number_param():

        return WorkerProtocolAnswer.get_number_param() + 3

    def get_sorted_param_list(self):

        return super(GetHostAnswer, self).get_sorted_param_list() + [self.HOST, self.USER_AGENT, self.VPN]


class SetHostAnswer(WorkerProtocolAnswer):

    def __init__(self):

        WorkerProtocolAnswer.__init__(self, "SET HOST")

    @staticmethod
    def get_number_param():

        return WorkerProtocolAnswer.get_number_param() + 0


class GetContentAnswer(WorkerProtocolAnswer):

    def __init__(self, hash_, content):

        WorkerProtocolAnswer.__init__(self, "GET CONTENT")

        self.HASH = hash_
        self.CONTENT = content

    @staticmethod
    def get_number_param():

        return WorkerProtocolAnswer.get_number_param() + 2

    def get_sorted_param_list(self):

        return super(GetContentAnswer, self).get_sorted_param_list() + [self.HASH, self.CONTENT]


class SetContentAnswer(WorkerProtocolAnswer):

    def __init__(self):

        WorkerProtocolAnswer.__init__(self, "SET CONTENT")

    @staticmethod
    def get_number_param():

        return WorkerProtocolAnswer.get_number_param() + 0


class ManagementProtocolMessage:

    def __init__(self, msg_type):

        self.MSG_TYPE = msg_type

    @staticmethod
    def get_number_param():

        return 2  # 1 + EOF

    def get_sorted_param_list(self):

        return [self.MSG_TYPE]


class StopMessage(ManagementProtocolMessage):

    def __init__(self, time_):

        ManagementProtocolMessage.__init__(self, "STOP")
        self.TIME = time_

    @staticmethod
    def get_number_param():

        return ManagementProtocolMessage.get_number_param() + 1

    def get_sorted_param_list(self):

        return super(StopMessage, self).get_sorted_param_list() + [self.TIME]


class ResumeMessage(ManagementProtocolMessage):

    def __init__(self):

        ManagementProtocolMessage.__init__(self, "RESUME")

    @staticmethod
    def get_number_param():

        return ManagementProtocolMessage.get_number_param() + 0


class HelloMessage(ManagementProtocolMessage):

    def __init__(self):

        ManagementProtocolMessage.__init__(self, "HELLO")

    @staticmethod
    def get_number_param():

        return ManagementProtocolMessage.get_number_param() + 0


class ManagementProtocolAnswer:

    def __init__(self, msg_type):

        self.MSG_TYPE = msg_type

    @staticmethod
    def get_number_param():

        return 3  # 1 + EOF + ANS

    def get_sorted_param_list(self):

        return ["ANS", self.MSG_TYPE]


class StopAnswer(ManagementProtocolAnswer):

    def __init__(self):

        ManagementProtocolAnswer.__init__(self, "STOP")

    @staticmethod
    def get_number_param():

        return ManagementProtocolAnswer.get_number_param() + 0


class ResumeAnswer(ManagementProtocolAnswer):

    def __init__(self):

        ManagementProtocolAnswer.__init__(self, "RESUME")

    @staticmethod
    def get_number_param():

        return ManagementProtocolAnswer.get_number_param() + 0


class HelloAnswer(ManagementProtocolAnswer):

    def __init__(self, agent, key, agent_type):

        ManagementProtocolAnswer.__init__(self, "HELLO")

        self.AGENT = agent
        self.KEY = key
        self.AGENT_TYPE = agent_type

    @staticmethod
    def get_number_param():

        return ManagementProtocolAnswer.get_number_param() + 3

    def get_sorted_param_list(self):

        return super(HelloAnswer, self).get_sorted_param_list() + [self.AGENT, self.KEY, self.AGENT_TYPE]
