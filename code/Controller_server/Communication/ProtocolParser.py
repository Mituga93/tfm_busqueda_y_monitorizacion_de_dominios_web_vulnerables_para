from . import ProtocolModels
from .. import Log


class ServerProtocolParser:
    def __init__(self, source, key, agent_type):

        Log.comms_layer("INFO", "Creating ServerProtocolParser class with: (" + str(
            source) + ", " + key + ", " + agent_type + ")")

        self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                  0,  # 1 ANS GET HOST
                                  0,  # 2 SET HOST
                                  0,  # 3 ANS SET HOST
                                  0,  # 4 GET CONTENT
                                  0,  # 5 ANS GET CONTENT
                                  0,  # 6 SET CONTENT
                                  0,  # 7 ANS SET CONTENT
                                  0,  # 8 STOP
                                  0,  # 9 ANS STOP
                                  0,  # 10 RESUME
                                  0,  # 11 ANS RESUME
                                  1,  # 12 HELLO
                                  0]  # 13 ANS HELLO

        self.SOURCE = source
        self.KEY = key
        self.AGENT_TYPE = agent_type

    def parse_rcved_msg(self, msg):

        Log.comms_layer("INFO", "Parsing received message on ServerProtocolParser class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        msg_s = msg.split("__", 15)

        if len(msg_s) > 14 or len(msg_s) == 0:
            Log.comms_layer("WARNING",
                            "Ignored an impossible to parse message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ") and MSG: " + msg)
        elif msg_s[1] == "HELLO" and self.KEY == "":
            return self.rcved_hello(msg_s)
        elif msg_s[1] != self.KEY:
            raise (RuntimeError, "Key changed during dialog")
        elif msg_s[2] == "GET HOST":
            return self.rcved_get_host(msg_s)
        elif msg_s[2] == "SET HOST":
            return self.rcved_set_host(msg_s)
        elif msg_s[2] == "GET CONTENT":
            return self.rcved_get_content(msg_s)
        elif msg_s[2] == "SET CONTENT":
            return self.rcved_set_content(msg_s)
        elif msg_s[1] == "RESUME":
            return self.rcved_resume(msg_s)
        elif msg_s[1] == "STOP":
            return self.rcved_stop(msg_s)
        else:
            Log.comms_layer("WARNING",
                            "Ignored an impossible to identify message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ") and MSG: " + msg)

        return None

    def rcved_get_host(self, msg_s):

        if self.ALLOWED_MSG_STATE[0] != 1:
            Log.comms_layer("WARNING",
                            "Ignored a non expected GET HOST message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        elif len(msg_s) != ProtocolModels.GetHostMessage.get_number_param():
            Log.comms_layer("WARNING",
                            "Ignored an invalid GET HOST message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        else:
            Log.comms_layer("INFO", "Received a GET HOST message on ServerProtocolParser class with: (" + str(
                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      1,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      0,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
            return ProtocolModels.GetHostMessage(self.SOURCE, self.KEY)

        return None

    def rcved_set_host(self, msg_s):

        if self.ALLOWED_MSG_STATE[2] != 1:
            Log.comms_layer("WARNING",
                            "Ignored a non expected SET HOST message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        elif len(msg_s) != ProtocolModels.SetHostMessage.get_number_param():
            Log.comms_layer("WARNING",
                            "Ignored an invalid SET HOST message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        else:
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      1,  # 3 ANS SET HOST
                                      0,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      0,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
            Log.comms_layer("INFO", "Received a SET HOST message on ServerProtocolParser class with: (" + str(
                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
            return ProtocolModels.SetHostMessage(self.SOURCE, self.KEY, msg_s[3], msg_s[4], msg_s[5], msg_s[6],
                                                 msg_s[7], msg_s[8], msg_s[9], msg_s[10], msg_s[11])

        return None

    def rcved_get_content(self, msg_s):

        if self.ALLOWED_MSG_STATE[4] != 1:
            Log.comms_layer("WARNING",
                            "Ignored a non expected GET CONTENT message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        elif len(msg_s) != ProtocolModels.GetContentMessage.get_number_param():
            Log.comms_layer("WARNING",
                            "Ignored an invalid GET CONTENT message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        else:
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 GET CONTENT
                                      1,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      0,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
            Log.comms_layer("INFO", "Received a GET CONTENT message on ServerProtocolParser class with: (" + str(
                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
            return ProtocolModels.GetContentMessage(self.SOURCE, self.KEY)

        return None

    def rcved_set_content(self, msg_s):

        if self.ALLOWED_MSG_STATE[6] != 1:
            Log.comms_layer("WARNING",
                            "Ignored a non expected SET CONTENT message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        elif len(msg_s) != ProtocolModels.SetContentMessage.get_number_param():
            Log.comms_layer("WARNING",
                            "Ignored an invalid SET CONTENT message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        else:
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      1,  # 7 ANS SET CONTENT
                                      0,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
            Log.comms_layer("INFO", "Received a SET CONTENT message on ServerProtocolParser class with: (" + str(
                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
            return ProtocolModels.SetContentMessage(self.SOURCE, self.KEY, msg_s[3], msg_s[4], msg_s[5], msg_s[6],
                                                    msg_s[7])

        return None

    def rcved_stop(self, msg_s):

        if self.ALLOWED_MSG_STATE[9] != 1:
            Log.comms_layer("WARNING",
                            "Ignored a non expected STOP message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        elif len(msg_s) != ProtocolModels.StopAnswer.get_number_param():
            Log.comms_layer("WARNING", "Ignored an invalid STOP message on ServerProtocolParser class with: (" + str(
                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        else:
            Log.comms_layer("INFO", "Received a STOP message on ServerProtocolParser class with: (" + str(
                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      0,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      1,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
            return True

        return False

    def rcved_resume(self, msg_s):

        if self.ALLOWED_MSG_STATE[11] != 1:
            Log.comms_layer("WARNING",
                            "Ignored a non expected RESUME message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        elif len(msg_s) != ProtocolModels.ResumeAnswer.get_number_param():
            Log.comms_layer("WARNING", "Ignored an invalid RESUME message on ServerProtocolParser class with: (" + str(
                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        else:
            Log.comms_layer("INFO", "Received a RESUME message on ServerProtocolParser class with: (" + str(
                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      1,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
            return True

        return False

    def rcved_hello(self, msg_s):

        if self.ALLOWED_MSG_STATE[13] != 1:
            Log.comms_layer("WARNING",
                            "Ignored a non expected HELLO message on ServerProtocolParser class with: (" + str(
                                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        elif len(msg_s) != ProtocolModels.HelloAnswer.get_number_param():
            Log.comms_layer("WARNING", "Ignored an invalid HELLO message on ServerProtocolParser class with: (" + str(
                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
        else:
            Log.comms_layer("INFO", "Received a HELLO message on ServerProtocolParser class with: (" + str(
                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")
            self.KEY = msg_s[3]
            self.AGENT_TYPE = msg_s[2]
            self.set_normal_state()
            return ProtocolModels.HelloAnswer(msg_s[2], msg_s[3], msg_s[4])

        return False

    def construct_to_send_msg(self, msg_o):

        Log.comms_layer("INFO", "Constructing protocol message on ServerProtocolParser class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ") and MSG_TYPE: " + msg_o.MSG_TYPE)

        msg_s = msg_o.get_sorted_param_list()

        if msg_o.MSG_TYPE == "GET HOST":
            self.ALLOWED_MSG_STATE = [1,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      1,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      0,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
        elif msg_o.MSG_TYPE == "SET HOST":
            self.ALLOWED_MSG_STATE = [1,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      1,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      0,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
        elif msg_o.MSG_TYPE == "GET CONTENT":
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      1,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      1,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      0,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
        elif msg_o.MSG_TYPE == "SET CONTENT":
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      1,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      1,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      0,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
        elif msg_o.MSG_TYPE == "STOP":
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      0,  # 8 STOP
                                      1,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
        elif msg_o.MSG_TYPE == "RESUME":
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      0,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      1,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
        elif msg_o.MSG_TYPE == "HELLO":
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      0,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      1]  # 13 ANS HELLO

        msg = ""
        for msg_p in msg_s:
            msg += msg_p + "__"

        msg += "\r\n\r\n"

        return msg

    def set_normal_state(self):

        Log.comms_layer("INFO", "Recovering to normal status on ServerProtocolParser class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        self.ALLOWED_MSG_STATE[6] = 0
        if self.AGENT_TYPE == "CRAWLER":
            self.ALLOWED_MSG_STATE = [1,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      0,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      1,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
        else:
            self.ALLOWED_MSG_STATE = [0,  # 0 GET HOST
                                      0,  # 1 ANS GET HOST
                                      0,  # 2 SET HOST
                                      0,  # 3 ANS SET HOST
                                      1,  # 4 GET CONTENT
                                      0,  # 5 ANS GET CONTENT
                                      0,  # 6 SET CONTENT
                                      0,  # 7 ANS SET CONTENT
                                      1,  # 8 STOP
                                      0,  # 9 ANS STOP
                                      0,  # 10 RESUME
                                      0,  # 11 ANS RESUME
                                      0,  # 12 HELLO
                                      0]  # 13 ANS HELLO
