import socket
import ssl
from .. import Log


class ServerSecureSocket:

    def __init__(self, ip, port):

        Log.comms_layer("INFO", "Creating ServerSecureSocket class with: (" + ip + ", " + str(port) + ")")

        self.IP = ip
        self.PORT = port

        self.SECURE_SOCKET = ssl.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM), certfile="/opt/EK_Crawler/ControllerServer/ssl/cert.pem", server_side=True, do_handshake_on_connect=False)

        self.SECURE_SOCKET.bind((self.IP, self.PORT))
        self.SECURE_SOCKET.listen(50)
        self.SECURE_SOCKET.setblocking(False)

    def accept(self):

        Log.comms_layer("INFO", "Accepting new connection on ServerSecureSocket class")

        sock = self.SECURE_SOCKET.accept()

        sock[0].do_handshake(True)

        return sock

    def close(self):

        Log.comms_layer("INFO", "Closing ServerSecureSocket")

        self.SECURE_SOCKET.close()


class AgentSecureSocket:

    def __init__(self, sock, source):

        Log.comms_layer("INFO", "Creating new AgentSecureSocket class from: " + str(source))

        self.SECURE_SOCKET = sock
        self.SOURCE = source
        self.SECURE_SOCKET.setblocking(True)

    def send_all(self, msg_raw):

        if len(msg_raw) > 100:
            Log.comms_layer("INFO", "Sending message: " + msg_raw[:100] + "...")
        else:
            Log.comms_layer("INFO", "Sending message: " + msg_raw)

        msg = msg_raw.encode('utf-8')

        length = str(len(msg)).zfill(7)
        msg = length.encode('utf-8') + msg

        total_sent = 0
        while total_sent < len(msg):

            sent = self.SECURE_SOCKET.send(msg[total_sent:])
            if sent == 0:
                Log.comms_layer("ERROR", "Connection closed while trying to send a message")
                raise Exception("Connection closed")
            total_sent += sent

    def rcv_all(self):

        received_size = 0
        s_size = bytes("", "UTF-8")
        while not received_size >= 7:

            chunk = self.SECURE_SOCKET.recv(7 - received_size)
            chunk_size = len(chunk)

            if chunk_size < 1:
                Log.comms_layer("ERROR", "Connection closed while trying to receive a message")
                raise Exception("Connection closed")

            received_size += chunk_size
            s_size = s_size + chunk

        size = int(s_size.decode("utf-8"))

        Log.comms_layer("INFO", "Expecting message of size: " + str(size))
        received_size = 0
        msg = bytes("", "UTF-8")
        while not received_size >= size:

            chunk = self.SECURE_SOCKET.recv(size - received_size)
            chunk_size = len(chunk)

            if chunk_size < 1:
                raise Exception("Connection closed")

            received_size += chunk_size
            msg = msg + chunk

        msg_decoded = msg.decode("utf-8")

        if len(msg_decoded) > 100:
            Log.comms_layer("INFO", "Received message: " + msg_decoded[:100] + "...")
        else:
            Log.comms_layer("INFO", "Received message: " + msg_decoded)

        return msg_decoded

    def close(self):

        self.SECURE_SOCKET.close()

        Log.comms_layer("INFO", "Closing AgentSecureSocket from: " + str(self.SOURCE))


class TestSecureSocket:

    def __init__(self):

        try:

            self.SECURE_SOCKET = ssl.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM), server_side=False, do_handshake_on_connect=False)
            self.SECURE_SOCKET.connect(("127.0.0.1", 8888))

            self.SECURE_SOCKET.do_handshake(True)
            self.SECURE_SOCKET.setblocking(True)

        except Exception as e:
            print(str(e))

    def close(self):

        self.SECURE_SOCKET.close()
