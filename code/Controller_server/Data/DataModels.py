
class IndexerCVEEntry:

    def __init__(self, cve, product, vendor, version):

        self.CVE = cve
        self.PRODUCT = product
        self.VENDOR = vendor
        self.VERSION = version


class IndexerKeywordsEntry:

    def __init__(self, id_, date, keyword, enabled):

        self.ID = id_
        self.DATE = date
        self.KEYWORD = keyword
        self.ENABLED = enabled


class IndexerHostsEntry:

    def __init__(self, ip, id_kw=None, cve=None, enabled=None):

        self.IP = ip
        self.ID_KW = id_kw
        self.CVE = cve
        self.ENABLED = enabled


class CrawlerUserAgentsEntry:

    def __init__(self, user_agent, id_=None, browser=None, browser_version=None, so=None, so_version=None, enabled=None):

        self.ID = id_
        self.BROWSER = browser
        self.BROWSER_VERSION = browser_version
        self.SO = so
        self.SO_VERSION = so_version
        self.USER_AGENT = user_agent
        self.ENABLED = enabled


class CrawlerVPNEntry:

    def __init__(self, config_file, country=None, enabled=None):

        self.COUNTRY = country
        self.CONFIG_FILE = config_file
        self.ENABLED = enabled


class CrawlerHostsEntry:

    def __init__(self, content_hash, content=None, ip=None, path=None, referrer=None, last_check=None, id_ua=None, country_vpn=None):

        self.CONTENT_HASH = content_hash
        self.IP = ip
        self.PATH = path
        self.REFERRER = referrer
        self.LAST_CHECK = last_check
        self.ID_UA = id_ua
        self.COUNTRY_VPN = country_vpn
        self.CONTENT = content


class AnalyzerPluginsEntry:

    def __init__(self, name, enabled=True):

        self.NAME = name
        self.ENABLED = enabled


class AnalyzerReportEntry:

    def __init__(self, hash_, plugin, date, report, description, id_=""):

        self.ID = id_
        self.HASH = hash_
        self.PLUGIN = plugin
        self.DATE = date
        self.REPORT = report
        self.DESCRIPTION = description


class SystemKeyEntry:

    def __init__(self, key, agent, ip, date, state):

        self.KEY = key
        self.AGENT = agent
        self.IP = ip
        self.DATE = date
        self.STATE = state
