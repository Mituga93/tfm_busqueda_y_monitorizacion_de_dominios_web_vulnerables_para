import psycopg2
from . import DataModels
from .. import Log


class GenericDBInterface:
    def __init__(self):

        Log.data_layer("INFO", "Creating GenericDBInterface")

        connect_str = "dbname='EK_Crawler' user='postgres' host='localhost' port='5433' password='ControllerServerAutomaticPassw0rd'"

        self.DB_CONN = psycopg2.connect(connect_str)

        self.DB_CURSOR1 = self.DB_CONN.cursor()

    def execute(self, query, params=()):

        second_try = False

        while True:
            try:
                self.DB_CURSOR1.execute(query, params)
                return self.DB_CURSOR1.fetchall()
            except Exception as e:
                if second_try:
                    Log.data_layer("ERROR", "Error executing a fetch query: " + str(e))
                    return ""
                else:
                    self.DB_CONN.rollback()
                    second_try = True

    def execute_non_fetch(self, query, params=()):

        try:
            self.DB_CURSOR1.execute(query, params)
            self.DB_CONN.commit()
        except Exception as e:

            Log.data_layer("ERROR", "Error executing a fetch query: " + str(e))

    def close(self):

        Log.data_layer("INFO", "Closing GenericDBInterface")

        self.DB_CURSOR1.close()
        self.DB_CONN.close()

    def validate_key(self, system_key_entry):

        rows = self.execute("SELECT key FROM System_keys WHERE key = %s and agent = %s and ip = %s",
                            (system_key_entry.KEY, system_key_entry.AGENT, system_key_entry.IP))

        validated = rows.length() > 0

        Log.data_layer("INFO", "Validating key with result: " + validated)

        return validated


class CrawlerManagerDBInterface(GenericDBInterface):
    def __init__(self):

        GenericDBInterface.__init__(self)

        self.HOSTS_CACHE = []

    def get_host(self):

        Log.data_layer("INFO", "Executing GET HOST query")

        if len(self.HOSTS_CACHE) == 0:
            rows = GenericDBInterface.execute(self,
                                              "SELECT ih.ip, cua.id, cua.user_agent, cvpn.country, cvpn.config_file FROM Indexer_hosts ih CROSS JOIN Crawler_user_agents cua CROSS JOIN Crawler_VPN cvpn WHERE ih.enabled AND cua.enabled AND cvpn.enabled ORDER BY ih.last_send IS NULL DESC, ih.last_send ASC LIMIT 20")

            if len(rows) == 0:
                Log.data_layer("ERROR", "GET HOST query returned 0 hosts to crawl")
                return "", "", ""

            for row in rows:
                indexer_hosts_entry = DataModels.IndexerHostsEntry(row[0])
                crawler_user_agent_entry = DataModels.CrawlerUserAgentsEntry(row[2], row[1])
                crawler_vpn_entry = DataModels.CrawlerVPNEntry(row[4], row[3])

                self.HOSTS_CACHE.append((indexer_hosts_entry, crawler_user_agent_entry, crawler_vpn_entry))

        current_host = (
            self.HOSTS_CACHE[0][0].IP, self.HOSTS_CACHE[0][1].USER_AGENT, self.HOSTS_CACHE[0][2].CONFIG_FILE)
        self.HOSTS_CACHE.remove(self.HOSTS_CACHE[0])

        GenericDBInterface.execute_non_fetch(self,
                                             "UPDATE Indexer_hosts SET last_send = current_timestamp WHERE ip = %s",
                                             (current_host[0],))

        return current_host

    def set_host(self, crawler_hosts_entry, crawler_user_agents_entry, crawler_vpn_entry):

        Log.data_layer("INFO", "Executing SET HOST query")

        crawler_hosts_entry.ID_UA = \
            GenericDBInterface.execute(self, "SELECT id FROM Crawler_user_agents WHERE user_agent = %s LIMIT 1",
                                       (crawler_user_agents_entry.USER_AGENT,))[0][0]
        crawler_hosts_entry.COUNTRY_VPN = \
            GenericDBInterface.execute(self, "SELECT country FROM Crawler_VPN WHERE config_file = %s LIMIT 1",
                                       (crawler_vpn_entry.CONFIG_FILE,))[0][0]

        GenericDBInterface.execute_non_fetch(self,
                                             "INSERT INTO Crawler_hosts (content_hash, ip, path, referrer, id_ua, country_vpn, content) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT (content_hash) DO UPDATE SET last_send = current_timestamp, count = crawler_hosts.count + 1",
                                             (crawler_hosts_entry.CONTENT_HASH, crawler_hosts_entry.IP,
                                              crawler_hosts_entry.PATH, crawler_hosts_entry.REFERRER,
                                              crawler_hosts_entry.ID_UA, crawler_hosts_entry.COUNTRY_VPN,
                                              crawler_hosts_entry.CONTENT))


class AnalyzerManagerDBInterface(GenericDBInterface):
    def __init__(self):

        GenericDBInterface.__init__(self)

        self.CONTENTS_CACHE = []

    def get_content(self):

        Log.data_layer("INFO", "Executing GET CONTENT query")

        if len(self.CONTENTS_CACHE) == 0:
            rows = GenericDBInterface.execute(self,
                                              "SELECT ch.content_hash, ch.content, ap.name FROM Crawler_hosts ch CROSS JOIN Analyzer_plugins ap WHERE ap.enabled AND ch.content_hash NOT IN (SELECT ar.hash FROM Analyzer_reports ar WHERE ar.hash = ch.content_hash and ar.plugin = ap.name) ORDER BY ch.last_check IS NULL DESC, ch.last_check ASC LIMIT 20")

            for row in rows:
                crawler_hosts_entry = DataModels.CrawlerHostsEntry(row[0], row[1])
                analyzer_plugins_entry = DataModels.AnalyzerPluginsEntry(row[2])

                self.CONTENTS_CACHE.append((crawler_hosts_entry, analyzer_plugins_entry))

        if len(self.CONTENTS_CACHE) == 0:
            return "EMPTY"

        current_content = (
            self.CONTENTS_CACHE[0][0].CONTENT_HASH, self.CONTENTS_CACHE[0][0].CONTENT, self.CONTENTS_CACHE[0][1].NAME)
        self.CONTENTS_CACHE.remove(self.CONTENTS_CACHE[0])

        GenericDBInterface.execute_non_fetch(self,
                                             "UPDATE Crawler_hosts SET last_check = current_timestamp WHERE content_hash = %s",
                                             (current_content[0],))

        return current_content

    def set_content(self, analyzer_reports_entry ):

        Log.data_layer("INFO", "Executing SET HOST query")

        GenericDBInterface.execute_non_fetch(self,
                                             "INSERT INTO Analyzer_reports (hash, plugin, report, description) VALUES (%s, %s, %s, %s)",
                                             (analyzer_reports_entry.HASH, analyzer_reports_entry.PLUGIN,
                                              analyzer_reports_entry.REPORT, analyzer_reports_entry.DESCRIPTION))
        # GenericDBInterface.execute_non_fetch(self, "UPDATE crawler_hosts SET last_check = now() WHERE content_hash = %s", (analyzer_reports_entry.HASH,))
