from ..Communication import SecureSocket
from ..Data import DBInterface
from . import Managers
from . import BusinessModels
from .. import Log
import signal
import time
import sys
try:
    import selectors
except ImportError:
    import selectors34 as selectors


class MainServer:

    def __init__(self, ip, port):

        signal.signal(signal.SIGINT, self.sig_kill)
        signal.signal(signal.SIGTERM, self.sig_kill)

        Log.business_layer("INFO", "Creating MainServer class on: (" + str(ip) + ":" + str(port) + ")")

        self.CRAWLER_MANAGER = Managers.CrawlerManager(self)
        self.ANALYZER_MANAGER = Managers.AnalyzerManager(self)

        self.SERVER_SOCKET = SecureSocket.ServerSecureSocket(ip, port)

        self.DB_INTERFACE = DBInterface.GenericDBInterface()

        self.UNKNOWN_AGENTS = [None, None, None, None, None, None, None, None, None, None,
                               None, None, None, None, None, None, None, None, None, None,
                               None, None, None, None, None, None, None, None, None, None,
                               None, None, None, None, None, None, None, None, None, None,
                               None, None, None, None, None, None, None, None, None, None]

        self.SELECTOR = selectors.DefaultSelector()

        self.SELECTOR.register(self.SERVER_SOCKET.SECURE_SOCKET, selectors.EVENT_READ, (-1, self.SERVER_SOCKET))

        self.SIG_KILL = False

    def sig_kill(self, signum, frame):

        self.SIG_KILL = True

        self.CRAWLER_MANAGER.send_stop()
        self.ANALYZER_MANAGER.send_stop()
        stop_init = time.time()

        Log.business_layer("ERROR", "SIGKILL received, starting the graceful shutdown")

        self.SERVER_SOCKET.close()
        for agent in self.UNKNOWN_AGENTS:
            if agent is not None:
                agent.UNKNOWN_SOCKET.close()

        while (not self.CRAWLER_MANAGER.check_stopped() and not self.ANALYZER_MANAGER.check_stopped()) or (time.time() - stop_init) < 20:
            Log.business_layer("ERROR", "Waiting for managers to be stopped, CRAWLER: [" + str(self.CRAWLER_MANAGER.STOP_COMPLETED) + "] ANALYZER: [" +
                               str(self.ANALYZER_MANAGER.STOP_COMPLETED) + "] seconds remaining [" + str(20 - round(time.time() - stop_init, 0)) + "]")
            time.sleep(5)

        self.CRAWLER_MANAGER.STOP_COMPLETED = True
        self.ANALYZER_MANAGER.STOP_COMPLETED = True
        time.sleep(3)
        Log.business_layer("ERROR", "Shutdown completed (could be forced, check previous log)")
        sys.exit(0)

    def run(self):

        Log.business_layer("INFO", "Running MainServer class")

        self.CRAWLER_MANAGER.start()
        self.ANALYZER_MANAGER.start()

        while True:

            Log.business_layer("INFO", "Multiplexing MainServer")

            events = self.SELECTOR.select(5)

            if self.SIG_KILL:
                self.sig_kill(None, None)

            for key, mask in events:

                sock_id = key.data[0]
                object_ = key.data[1]

                Log.business_layer("INFO", "Event trigger detected on sock_id: " + str(sock_id))

                if sock_id < 0:  # Case of server listen socket

                    new_socket, source = object_.accept()

                    idx = 0

                    for unknown_agent in self.UNKNOWN_AGENTS:

                        if unknown_agent is None:

                            Log.business_layer("INFO", "Saving new unidentified connection in position: " + str(idx))

                            unknown_agent = BusinessModels.UnknownAgent(source, self.DB_INTERFACE, SecureSocket.AgentSecureSocket(new_socket, source))
                            self.UNKNOWN_AGENTS[sock_id] = unknown_agent
                            unknown_agent.send_hello()
                            self.SELECTOR.register(unknown_agent.UNKNOWN_SOCKET.SECURE_SOCKET, selectors.EVENT_READ, (idx, unknown_agent))

                            break

                        idx += 1

                    if idx == len(self.UNKNOWN_AGENTS):
                        Log.business_layer("WARNING", "New unidentified connection discarded because of full buffer")
                        new_socket.close()

                else:  # Case of unidentified socket

                    self.SELECTOR.unregister(object_.UNKNOWN_SOCKET.SECURE_SOCKET)
                    self.UNKNOWN_AGENTS[sock_id] = None

                    if object_.event_received():

                        key = object_.PROTOCOL_PARSER.KEY
                        agent = object_.MODULE
                        type_ = object_.TYPE
                        source = object_.PROTOCOL_PARSER.SOURCE

                        Log.business_layer("INFO", "Registering unidentified agent HELLO as: (" + key + ", " + agent + ", " + type_ + ", " + str(source) + ")")

                        if agent == "CRAWLER" and type_ == "MANAGEMENT":
                            self.CRAWLER_MANAGER.register_management_socket(source, key, object_.UNKNOWN_SOCKET, object_.PROTOCOL_PARSER)
                        elif agent == "CRAWLER" and type_ == "WORKER":
                            self.CRAWLER_MANAGER.register_worker_socket(source, key, object_.UNKNOWN_SOCKET, object_.PROTOCOL_PARSER)
                        elif agent == "ANALYZER" and type_ == "MANAGEMENT":
                            self.ANALYZER_MANAGER.register_management_socket(source, key, object_.UNKNOWN_SOCKET, object_.PROTOCOL_PARSER)
                        elif agent == "ANALYZER" and type_ == "WORKER":
                            self.ANALYZER_MANAGER.register_worker_socket(source, key, object_.UNKNOWN_SOCKET, object_.PROTOCOL_PARSER)
                        else:
                            object_.UNKNOWN_SOCKET.close()
                            Log.business_layer("WARNING", "Impossible to classify agent as: (" + key + ", " + agent + ", " + type_ + ", " + str(source) + ")")
                    else:
                        object_.UNKNOWN_SOCKET.close()
                        Log.business_layer("WARNING", "Unidentified connection discarded because of protocol violation")
