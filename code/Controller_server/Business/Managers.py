
from . import BusinessModels
from ..Data import DBInterface
from ..Communication import SecureSocket
from .. import Log
import threading
import time
import signal
try:
    import selectors
except ImportError:
    import selectors34 as selectors


class GenericManager(threading.Thread):

    def __init__(self, parent):

        Log.business_layer("INFO", "Creating GenericManager class")

        threading.Thread.__init__(self)
        self.REGISTERED_AGENTS = []
        self.SELECTOR = selectors.DefaultSelector()
        self.DB_INTERFACE = None

        self.PARENT = parent

        signal.signal(signal.SIGINT, self.sig_kill)
        signal.signal(signal.SIGTERM, self.sig_kill)

        self.STOP_SEND = False
        self.STOP_COMPLETED = False

    def sig_kill(self, signum, frame):

        self.PARENT.SIG_KILL = True

    def send_stop(self):

        self.STOP_SEND = True

        for agent in self.REGISTERED_AGENTS:

            agent.send_stop()

    def check_stopped(self):

        for agent in self.REGISTERED_AGENTS:

            if not agent.STOPPED:
                return False

        self.STOP_COMPLETED = True
        return True

    def run(self):

        Log.business_layer("INFO", "Running GenericManager class")

        while not self.STOP_COMPLETED:

            sock_id = -2
            agent = None
            Log.business_layer("INFO", "Multiplexing GenericManager class")

            try:

                events = self.SELECTOR.select()

                for key, mask in events:

                    (sock_id, agent) = key.data

                    Log.business_layer("INFO", "Event trigger detected on sock_id: " + str(sock_id))

                    agent.event_received(sock_id)

            except Exception as e:

                if e.args[0] == "maxevents must be greater than 0, got 0":
                    Log.business_layer("INFO", "There are no sockets registered to multiplex in GenericManager")
                elif e.args[0] == "Connection closed":
                    Log.business_layer("ERROR", "Connection error on AGENT MANAGER with ID: " + str(sock_id))
                    affected_sockets = agent.handle_connection_closed()
                    for s in affected_sockets:
                        self.SELECTOR.unregister(s.SECURE_SOCKET)

                else:
                    Log.business_layer("ERROR", "Unknown error on AGENT MANAGER with ID: " + str(sock_id) + " Error: " + str(e))
                    #raise(e)
                time.sleep(2)


class CrawlerManager(GenericManager):

    def __init__(self, parent):

        GenericManager.__init__(self, parent)
        self.DB_INTERFACE = DBInterface.CrawlerManagerDBInterface()

    def register_management_socket(self, source, key, socket, protocol_parser):

        found = False

        for agent in self.REGISTERED_AGENTS:

            if agent.match_agent(source, key):

                Log.business_layer("INFO", "Registering new management_socket to an existing agent: (" + str(source) + ", " + key + ")")

                self.SELECTOR.unregister(agent.set_management_socket(socket, protocol_parser).SECURE_SOCKET)
                self.SELECTOR.register(socket.SECURE_SOCKET, selectors.EVENT_READ, (-1, agent))
                found = True
                break

        if not found:

            Log.business_layer("INFO", "Registering new management_socket in a new agent: (" + str(source) + ", " + key + ")")

            agent = BusinessModels.CrawlerAgent(source, key, self.DB_INTERFACE)
            agent.set_management_socket(socket, protocol_parser)
            self.SELECTOR.register(socket.SECURE_SOCKET, selectors.EVENT_READ, (-1, agent))

    def register_worker_socket(self, source, key, socket, protocol_parser):

        Log.business_layer("INFO", "Registering new worker_socket: (" + str(source) + ", " + key + ")")

        found = False

        for agent in self.REGISTERED_AGENTS:

            if agent.match_agent(source, key):

                Log.business_layer("INFO", "Registering new worker_socket in an existing agent: (" + str(source) + ", " + key + ")")

                self.SELECTOR.unregister(socket.SECURE_SOCKET)
                self.SELECTOR.register(socket.SECURE_SOCKET, selectors.EVENT_READ, (agent.add_worker_socket(socket, protocol_parser), agent))
                found = True
                break

        if not found:

            Log.business_layer("INFO", "Registering new worker_socket in a new agent: (" + str(source) + ", " + key + ")")

            agent = BusinessModels.CrawlerAgent(source, key, self.DB_INTERFACE)
            agent.add_worker_socket(socket, protocol_parser)
            self.SELECTOR.register(socket.SECURE_SOCKET, selectors.EVENT_READ, (0, agent))


class AnalyzerManager(GenericManager):

    def __init__(self, parent):

        GenericManager.__init__(self, parent)
        self.DB_INTERFACE = DBInterface.AnalyzerManagerDBInterface()

    def register_management_socket(self, source, key, socket, protocol_parser):

        found = False

        for agent in self.REGISTERED_AGENTS:

            if agent.match_agent(source, key):

                Log.business_layer("INFO", "Registering new management_socket to an existing agent: (" + str(source) + ", " + key + ")")

                self.SELECTOR.unregister(agent.set_management_socket(socket, protocol_parser).SECURE_SOCKET)
                self.SELECTOR.register(socket.SECURE_SOCKET, selectors.EVENT_READ, (-1, agent))
                found = True
                break

        if not found:

            Log.business_layer("INFO", "Registering new management_socket in a new agent: (" + str(source) + ", " + key + ")")

            agent = BusinessModels.AnalyzerAgent(source, key, self.DB_INTERFACE)
            agent.set_management_socket(socket, protocol_parser)
            self.SELECTOR.register(socket.SECURE_SOCKET, selectors.EVENT_READ, (-1, agent))

    def register_worker_socket(self, source, key, socket, protocol_parser):

        Log.business_layer("INFO", "Registering new worker_socket: (" + str(source) + ", " + key + ")")

        found = False

        for agent in self.REGISTERED_AGENTS:

            if agent.match_agent(source, key):

                Log.business_layer("INFO", "Registering new worker_socket in an existing agent: (" + str(source) + ", " + key + ")")

                self.SELECTOR.unregister(socket.SECURE_SOCKET)
                self.SELECTOR.register(socket.SECURE_SOCKET, selectors.EVENT_READ, (agent.add_worker_socket(socket, protocol_parser), agent))
                found = True
                break

        if not found:

            Log.business_layer("INFO", "Registering new worker_socket in a new agent: (" + str(source) + ", " + key + ")")

            agent = BusinessModels.AnalyzerAgent(source, key, self.DB_INTERFACE)
            agent.add_worker_socket(socket, protocol_parser)
            self.SELECTOR.register(socket.SECURE_SOCKET, selectors.EVENT_READ, (0, agent))
