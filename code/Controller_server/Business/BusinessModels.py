from ..Communication import ProtocolParser
from ..Communication import ProtocolModels
from ..Data import DataModels
from .. import Log


class UnknownAgent:
    def __init__(self, source, db_interface, socket):

        Log.business_layer("INFO", "Creating new UnknownAgent class from: " + str(source))

        self.SOURCE = source
        self.PROTOCOL_PARSER = ProtocolParser.ServerProtocolParser(source, "", "")
        self.DB_INTERFACE = db_interface
        self.UNKNOWN_SOCKET = socket
        self.MODULE = ""
        self.TYPE = ""

    def send_hello(self):

        Log.business_layer("INFO", "Sending HELLO to agent in: " + str(self.SOURCE))

        self.UNKNOWN_SOCKET.send_all(self.PROTOCOL_PARSER.construct_to_send_msg(ProtocolModels.HelloMessage()))

    def event_received(self):

        Log.business_layer("INFO", "Expecting HELLO answer from agent in: " + str(self.SOURCE))

        socket = self.UNKNOWN_SOCKET
        msg = socket.rcv_all()

        object_received = self.PROTOCOL_PARSER.parse_rcved_msg(msg)

        if object_received.MSG_TYPE == "HELLO":

            Log.business_layer("INFO",
                               "Received HELLO with: (" + object_received.AGENT + ", " + object_received.AGENT_TYPE + ") from: " + str(
                                   self.SOURCE))

            self.MODULE = object_received.AGENT
            self.TYPE = object_received.AGENT_TYPE
            return True
        else:

            Log.business_layer("WARNING",
                               "Received an unidentified message while expecting HELLO answer: " + object_received.MSG_TYPE)

            return False


class GenericAgent:
    def __init__(self, source, key, agent_type, db_interface, ):

        self.MANAGEMENT_SOCKET = None
        self.WORKER_SOCKETS = []
        self.SOURCE = source
        self.KEY = key
        self.AGENT_TYPE = agent_type
        self.DB_INTERFACE = db_interface

        self.MANAGEMENT_PROTOCOL_PARSER = None
        self.WORKER_PROTOCOL_PARSER = []

        self.STOPPED = False

        Log.business_layer("INFO",
                           "Creating new Agent class with: (" + str(source) + ", " + key + ", " + agent_type + ")")

    def match_agent(self, source, key):

        return source == self.SOURCE and key == self.KEY

    def set_management_socket(self, socket, protocol_parser):

        Log.business_layer("INFO", "Setting management_socket of Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        old_socket = self.MANAGEMENT_SOCKET
        self.MANAGEMENT_SOCKET = socket
        self.MANAGEMENT_PROTOCOL_PARSER = protocol_parser
        return old_socket

    def add_worker_socket(self, socket, protocol_parser):

        Log.business_layer("INFO", "Adding worker_socket to Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        self.WORKER_SOCKETS.append(socket)
        self.WORKER_PROTOCOL_PARSER.append(protocol_parser)

        return len(self.WORKER_SOCKETS) - 1

    def event_received(self, sock_id):

        Log.business_layer("INFO", "Message received on Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ") and ID: " + str(sock_id))

        if sock_id < 0:
            socket = self.MANAGEMENT_SOCKET
            protocol_parser = self.MANAGEMENT_PROTOCOL_PARSER
        else:
            socket = self.WORKER_SOCKETS[sock_id]
            protocol_parser = self.WORKER_PROTOCOL_PARSER[sock_id]

        msg = socket.rcv_all()

        object_received = protocol_parser.parse_rcved_msg(msg)

        if object_received is None:
            Log.business_layer("DEBUG", "BussinessModels --> GenericAgent --> event_received --> if object_received is none")
            return None, None

        Log.business_layer("INFO", "Message received on Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ") and MSG_TYPE: " + object_received.MSG_TYPE)

        if object_received.MSG_TYPE == "RESUME":
            self.resume()
        elif object_received.MSG_TYPE == "STOP":
            self.stop()
        else:
            return socket, object_received

        return None, None

    def send_stop(self):

        Log.business_layer("INFO", "Sending STOP on Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        self.MANAGEMENT_SOCKET.send_all(
            self.MANAGEMENT_PROTOCOL_PARSER.construct_to_send_msg(ProtocolModels.StopMessage(0)))

    def send_resume(self):

        Log.business_layer("INFO", "Sending RESUME on Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        self.MANAGEMENT_SOCKET.send_all(
            self.MANAGEMENT_PROTOCOL_PARSER.construct_to_send_msg(ProtocolModels.ResumeMessage()))

    def resume(self):

        Log.business_layer("INFO", "Received RESUME answer on Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        self.STOPPED = False

        return

    def stop(self):

        Log.business_layer("INFO", "Received STOP answer on Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        self.STOPPED = True

        return

    def handle_connection_closed(self):

        Log.business_layer("ERROR", "Closing every socket due to an error handling for connection closed on Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        affected_sockets = []

        for s in self.WORKER_SOCKETS:
            if s is not None:
                s.close()
                affected_sockets.append(s)
        if self.MANAGEMENT_SOCKET is not None:
            self.MANAGEMENT_SOCKET.close()
            affected_sockets.append(self.MANAGEMENT_SOCKET)

        return affected_sockets


class CrawlerAgent(GenericAgent):
    def __init__(self, source, key, db_interface):

        GenericAgent.__init__(self, source, key, "CRAWLER", db_interface)

    def event_received(self, sock_id):

        socket, object_received = GenericAgent.event_received(self, sock_id)

        if socket is None:  # Event handled on parents function
            return
        if object_received.MSG_TYPE == "GET HOST":
            self.get_host(socket, sock_id)
        elif object_received.MSG_TYPE == "SET HOST":
            self.set_host(object_received, socket, sock_id)
        else:
            Log.business_layer("WARNING", "Ignored unidentified message received on Agent class with: (" + str(
                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ") and MSG_TYPE: " + object_received.MSG_TYPE)

    def get_host(self, socket, sock_id):

        Log.business_layer("INFO", "Handling GET HOST on Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        host = self.DB_INTERFACE.get_host()

        msg = self.WORKER_PROTOCOL_PARSER[sock_id].construct_to_send_msg(
            ProtocolModels.GetHostAnswer(host[0], host[1], host[2]))

        socket.send_all(msg)

    def set_host(self, set_host_message, socket, sock_id):

        Log.business_layer("INFO", "Handling SET HOST on Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        self.DB_INTERFACE.set_host(
            DataModels.CrawlerHostsEntry(set_host_message.HASH, content=set_host_message.CONTENT, ip=set_host_message.HOST,
                                         path=set_host_message.PATH, referrer=set_host_message.REFERRER),
            DataModels.CrawlerUserAgentsEntry(set_host_message.USER_AGENT),
            DataModels.CrawlerVPNEntry(set_host_message.VPN))

        msg = self.WORKER_PROTOCOL_PARSER[sock_id].construct_to_send_msg(ProtocolModels.SetHostAnswer())

        socket.send_all(msg)


class AnalyzerAgent(GenericAgent):

    def __init__(self, source, key, db_interface):

        GenericAgent.__init__(self, source, key, "ANALYZER", db_interface)

    def event_received(self, sock_id):

        socket, object_received = GenericAgent.event_received(self, sock_id)

        if socket is None:  # Event handled on parents function
            return
        if object_received.MSG_TYPE == "GET CONTENT":
            self.get_content(socket, sock_id)
        elif object_received.MSG_TYPE == "SET CONTENT":
            self.set_content(object_received, socket, sock_id)
        else:
            Log.business_layer("WARNING", "Ignored unidentified message received on Agent class with: (" + str(
                self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ") and MSG_TYPE: " + object_received.MSG_TYPE)

    def get_content(self, socket, sock_id):

        Log.business_layer("INFO", "Handling GET CONTENT on Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        content = self.DB_INTERFACE.get_content()

        msg = self.WORKER_PROTOCOL_PARSER[sock_id].construct_to_send_msg(
            ProtocolModels.GetContentAnswer(content[0], content[1]))

        socket.send_all(msg)

    def set_content(self, set_content_message, socket, sock_id):

        Log.business_layer("INFO", "Handling SET CONTENT on Agent class with: (" + str(
            self.SOURCE) + ", " + self.KEY + ", " + self.AGENT_TYPE + ")")

        self.DB_INTERFACE.set_content(
            DataModels.AnalyzerReportEntry(set_content_message.HASH, set_content_message.TEST, set_content_message.DATETIME, set_content_message.RESULT, set_content_message.DESC))

        msg = self.WORKER_PROTOCOL_PARSER[sock_id].construct_to_send_msg(ProtocolModels.SetContentAnswer())

        socket.send_all(msg)
