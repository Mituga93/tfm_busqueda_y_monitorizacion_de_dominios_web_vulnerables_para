import threading
import time
import datetime


def comms_layer(level, msg):

    timestamp = time.time()
    formatted_timestamp = str(datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S'))
    thread_id = str(threading.get_ident())

    log = "[" + formatted_timestamp + "] [" + thread_id + "] [" + level + "] [" + msg + "]"

    print(log)


def presentation_layer(level, msg):

    timestamp = time.time()
    formatted_timestamp = str(datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S'))
    thread_id = str(threading.get_ident())

    log = "[" + formatted_timestamp + "] [" + thread_id + "] [" + level + "] [" + msg + "]"

    print(log)


def business_layer(level, msg):

    timestamp = time.time()
    formatted_timestamp = str(datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S'))
    thread_id = str(threading.get_ident())

    log = "[" + formatted_timestamp + "] [" + thread_id + "] [" + level + "] [" + msg + "]"

    print(log)
