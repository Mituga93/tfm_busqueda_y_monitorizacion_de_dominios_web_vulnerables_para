from . import BusinessModels
from .. import Log
import threading


class MainAgent:

    def __init__(self, ip, port, key):

        Log.business_layer("INFO", "Creating MainAgent class with: (" + str(ip) + ":" + str(port) + ", " + key + ")")

        self.AGENT = BusinessModels.AnalyzerAgent(ip, port, key)

        # self.MANAGEMENT = threading.Thread(target=self.AGENT.start_management())
        self.WORKER = threading.Thread(target=self.AGENT.start_workers)

    def run(self):

        Log.business_layer("INFO", "Running MainAgent")

        # self.MANAGEMENT.run()
        self.WORKER.start()

        # self.AGENT.start_management()
