from .. import Log
import yara


class YarasHandler ():

    def __init__(self):
        self.EK_rules = yara.compile(filepath="Analyzer_agent/Business/yaras/index.yar")

    def match(self, data):
        try:
            matches = self.EK_rules.match(data=data)

            rules = ""

            if len(matches) > 0:
                c = str(len(matches["main"]))
            else:
                c = 0

            Log.business_layer("INFO", "Detected [" + str(c) + "] matches in the yara rules")

            if c == 0:
                return ""

            for item in matches["main"]:

                rules += item["rule"] + "|"

            if rules != "":
                rules = rules[:-1]

            return rules
        except Exception as e:

            return "ERROR"
