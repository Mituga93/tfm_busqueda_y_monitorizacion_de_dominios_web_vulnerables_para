/*
    This Yara ruleset is under the GNU-GPLv2 license (http://www.gnu.org/licenses/gpl-2.0.html) and open to any user or organization, as    long as you use it under this license.

*/
rule blackhole2_jar : EK
{
strings:
   $string0 = "k0/3;N"
   $string1 = "g:WlY0"
   $string2 = "(ww6Ou"
   $string3 = "SOUGX["
   $string4 = "7X2ANb"
   $string5 = "r8L<;zYH)"
   $string6 = "fbeatbea/fbeatbee.classPK"
   $string7 = "fbeatbea/fbeatbec.class"
   $string8 = "fbeatbea/fbeatbef.class"
   $string9 = "fbeatbea/fbeatbef.classPK"
   $string10 = "fbeatbea/fbeatbea.class"
   $string11 = "fbeatbea/fbeatbeb.classPK"
   $string12 = "nOJh-2"
   $string13 = "[af:Fr"
condition:
   13 of them
}
rule blackhole2_jar2 : EK
{
strings:
   $string0 = "6_O6d09"
   $string1 = "juqirvs.classPK"
   $string2 = "hw.classPK"
   $string3 = "a.classPK"
   $string4 = "w.classuS]w"
   $string5 = "w.classPK"
   $string6 = "YE}0vCZ"
   $string7 = "v)Q,Ff"
   $string8 = "%8H%t("
   $string9 = "hw.class"
   $string10 = "a.classmV"
   $string11 = "2CniYFU"
   $string12 = "juqirvs.class"
condition:
   12 of them
}
rule blackhole2_jar3 : EK
{
strings:
   $string0 = "69/sj]]o"
   $string1 = "GJk5Nd"
   $string2 = "vcs.classu"
   $string3 = "T<EssB"
   $string4 = "1vmQmQ"
   $string5 = "Kf1Ewr"
   $string6 = "c$WuuuKKu5"
   $string7 = "m.classPK"
   $string8 = "chcyih.classPK"
   $string9 = "hw.class"
   $string10 = "f';;;;{"
   $string11 = "vcs.classPK"
   $string12 = "Vbhf_6"
condition:
   12 of them
}
rule blackhole2_pdf : EK PDF
{
strings:
   $string0 = "/StructTreeRoot 5 0 R/Type/Catalog>>"
   $string1 = "0000036095 00000 n"
   $string2 = "http://www.xfa.org/schema/xfa-locale-set/2.1/"
   $string3 = "subform[0].ImageField1[0])/Subtype/Widget/TU(Image Field)/Parent 22 0 R/F 4/P 8 0 R/T<FEFF0049006D00"
   $string4 = "0000000026 65535 f"
   $string5 = "0000029039 00000 n"
   $string6 = "0000029693 00000 n"
   $string7 = "%PDF-1.6"
   $string8 = "27 0 obj<</Subtype/Type0/DescendantFonts 28 0 R/BaseFont/KLGNYZ"
   $string9 = "0000034423 00000 n"
   $string10 = "0000000010 65535 f"
   $string11 = ">stream"
   $string12 = "/Pages 2 0 R%/StructTreeRoot 5 0 R/Type/Catalog>>"
   $string13 = "19 0 obj<</Subtype/Type1C/Length 23094/Filter/FlateDecode>>stream"
   $string14 = "0000003653 00000 n"
   $string15 = "0000000023 65535 f"
   $string16 = "0000028250 00000 n"
   $string17 = "iceRGB>>>>/XStep 9.0/Type/Pattern/TilingType 2/YStep 9.0/BBox[0 0 9 9]>>stream"
   $string18 = "<</Root 1 0 R>>"
condition:
   18 of them
}
rule blackhole_basic :  EK
{
    strings:
        $a = /\.php\?\.*\?\:[a-zA-Z0-9\:]{6,}\&\.*\?\&/
    condition:
        $a
}
rule blackhole1_jar
{
strings:
   $string0 = "Created-By: 1.6.0_18 (Sun Microsystems Inc.)"
   $string1 = "workpack/decoder.classmQ]S"
   $string2 = "workpack/decoder.classPK"
   $string3 = "workpack/editor.classPK"
   $string4 = "xmleditor/GUI.classmO"
   $string5 = "xmleditor/GUI.classPK"
   $string6 = "xmleditor/peers.classPK"
   $string7 = "v(SiS]T"
   $string8 = ",R3TiV"
   $string9 = "META-INF/MANIFEST.MFPK"
   $string10 = "xmleditor/PK"
   $string11 = "Z[Og8o"
   $string12 = "workpack/PK"
condition:
   12 of them
}
rule blackhole2_css : EK
{
strings:
   $string1 = "background:url('%%?a=img&img=countries.gif')"
   $string2 = "background:url('%%?a=img&img=exploit.gif')"
   $string3 = "background:url('%%?a=img&img=oses.gif')"
   $string4 = "background:url('%%?a=img&img=browsers.gif')"
   $string5 = "background:url('%%?a=img&img=edit.png')"
   $string6 = "background:url('%%?a=img&img=add.png')"
   $string7 = "background:url('%%?a=img&img=accept.png')"
   $string8 = "background:url('%%?a=img&img=del.png')"
   $string9 = "background:url('%%?a=img&img=stat.gif')"
condition:
   18 of them
}
rule blackhole2_htm : EK
{
strings:
   $string0 = ">links/</a></td><td align"
   $string1 = ">684K</td><td>"
   $string2 = "> 36K</td><td>"
   $string3 = "move_logs.php"
   $string4 = "files/"
   $string5 = "cron_updatetor.php"
   $string6 = ">12-Sep-2012 23:45  </td><td align"
   $string7 = ">  - </td><td>"
   $string8 = "cron_check.php"
   $string9 = "-//W3C//DTD HTML 3.2 Final//EN"
   $string10 = "bhadmin.php"
   $string11 = ">21-Sep-2012 15:25  </td><td align"
   $string12 = ">data/</a></td><td align"
   $string13 = ">3.3K</td><td>"
   $string14 = "cron_update.php"
condition:
   14 of them
}
rule blackhole2_htm10 : EK
{
strings:
   $string0 = "</body></html>"
   $string1 = "/icons/back.gif"
   $string2 = ">373K</td><td>"
   $string3 = "/icons/unknown.gif"
   $string4 = ">Last modified</a></th><th><a href"
   $string5 = "tmp.gz"
   $string6 = ">tmp.gz</a></td><td align"
   $string7 = "nbsp;</td><td align"
   $string8 = "</table>"
   $string9 = ">  - </td><td>"
   $string10 = ">filefdc7aaf4a3</a></td><td align"
   $string11 = ">19-Sep-2012 07:06  </td><td align"
   $string12 = "><img src"
   $string13 = "file3fa7bdd7dc"
   $string14 = "  <title>Index of /files</title>"
   $string15 = "0da49e042d"
condition:
   15 of them
}
rule blackhole2_htm11 : EK
{
strings:
   $string0 = "></th><th><a href"
   $string1 = "/icons/back.gif"
   $string2 = ">Description</a></th></tr><tr><th colspan"
   $string3 = "nbsp;</td><td align"
   $string4 = "nbsp;</td></tr>"
   $string5 = ">  - </td><td>"
   $string6 = "-//W3C//DTD HTML 3.2 Final//EN"
   $string7 = "<h1>Index of /dummy</h1>"
   $string8 = ">Size</a></th><th><a href"
   $string9 = " </head>"
   $string10 = "/icons/blank.gif"
   $string11 = "><hr></th></tr>"
condition:
   11 of them
}
rule blackhole2_htm12 : EK
{
strings:
   $string0 = "  <title>Index of /data</title>"
   $string1 = "<tr><th colspan"
   $string2 = "</body></html>"
   $string3 = "> 20K</td><td>"
   $string4 = "/icons/layout.gif"
   $string5 = " <body>"
   $string6 = ">Name</a></th><th><a href"
   $string7 = ">spn.jar</a></td><td align"
   $string8 = ">spn2.jar</a></td><td align"
   $string9 = " <head>"
   $string10 = "-//W3C//DTD HTML 3.2 Final//EN"
   $string11 = "> 10K</td><td>"
   $string12 = ">7.9K</td><td>"
   $string13 = ">Size</a></th><th><a href"
   $string14 = "><hr></th></tr>"
condition:
   14 of them
}
rule blackhole2_htm3 : EK
{
strings:
   $string0 = "/download.php"
   $string1 = "./files/fdc7aaf4a3 md5 is 3169969e91f5fe5446909bbab6e14d5d"
   $string2 = "321e774d81b2c3ae"
   $string3 = "/files/new00010/554-0002.exe md5 is 8a497cf4ffa8a173a7ac75f0de1f8d8b"
   $string4 = "./files/3fa7bdd7dc md5 is 8a497cf4ffa8a173a7ac75f0de1f8d8b"
   $string5 = "1603256636530120915 md5 is 425ebdfcf03045917d90878d264773d2"
condition:
   3 of them
}
rule blackhole2_htm4 : EK
{
strings:
   $string0 = "words.dat"
   $string1 = "/icons/back.gif"
   $string2 = "data.dat"
   $string3 = "files.php"
   $string4 = "js.php"
   $string5 = "template.php"
   $string6 = "kcaptcha"
   $string7 = "/icons/blank.gif"
   $string8 = "java.dat"
condition:
   8 of them
}
rule blackhole2_htm5 : EK
{
strings:
   $string0 = "ruleEdit.php"
   $string1 = "domains.php"
   $string2 = "menu.php"
   $string3 = "browsers_stat.php"
   $string4 = "Index of /library/templates"
   $string5 = "/icons/unknown.gif"
   $string6 = "browsers_bstat.php"
   $string7 = "oses_stat.php"
   $string8 = "exploits_bstat.php"
   $string9 = "block_config.php"
   $string10 = "threads_bstat.php"
   $string11 = "browsers_bstat.php"
   $string12 = "settings.php"
condition:
   12 of them
}
rule blackhole2_htm6 : EK
{
strings:
   $string0 = "uniq1.png"
   $string1 = "edit.png"
   $string2 = "left.gif"
   $string3 = "infin.png"
   $string4 = "outdent.gif"
   $string5 = "exploit.gif"
   $string6 = "sem_g.png"
   $string7 = "Index of /library/templates/img"
   $string8 = "uniq1.png"
condition:
   8 of them
}
rule blackhole2_htm8 : EK
{
strings:
   $string0 = ">Description</a></th></tr><tr><th colspan"
   $string1 = ">Name</a></th><th><a href"
   $string2 = "main.js"
   $string3 = "datepicker.js"
   $string4 = "form.js"
   $string5 = "<address>Apache/2.2.15 (CentOS) Server at online-moo-viii.net Port 80</address>"
   $string6 = "wysiwyg.js"
condition:
   6 of them
}
