rule EK_common_sintaxis
{
    strings:

        $loops_count = /(for|while)/

	$function_1line = /(\){){1}[ \r\n\t]{0,4}(return){1}/

	$big_numbers = /\d{4,}/

	$encode_bug1 = "unescape(encodeURI("
	$encode_bug2 = "decodeURI(escape("

	$char_code_at = "charCodeAt("

	$non_printable_chars = /([^\x20-\x7E]+)/

    condition:
	//((#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  $encode_bug2) or (#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  #char_code_at > 2) or (#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  $non_printable_chars) or (#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  $encode_bug2 and  #char_code_at > 2) or (#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  $encode_bug2 and  $non_printable_chars) or (#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  #char_code_at > 2 and  $non_printable_chars) or (#loops_count > 5 and  #function_1line > 3 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2) or (#loops_count > 5 and  #function_1line > 3 and  $encode_bug1 and  $encode_bug2 and  $non_printable_chars) or (#loops_count > 5 and  #function_1line > 3 and  $encode_bug1 and  #char_code_at > 2 and  $non_printable_chars) or (#loops_count > 5 and  #function_1line > 3 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars) or (#loops_count > 5 and  #big_numbers > 5 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2) or (#loops_count > 5 and  #big_numbers > 5 and  $encode_bug1 and  $encode_bug2 and  $non_printable_chars) or (#loops_count > 5 and  #big_numbers > 5 and  $encode_bug1 and  #char_code_at > 2 and  $non_printable_chars) or (#loops_count > 5 and  #big_numbers > 5 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars) or (#loops_count > 5 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars) or (#function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2) or (#function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  $encode_bug2 and  $non_printable_chars) or (#function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  #char_code_at > 2 and  $non_printable_chars) or (#function_1line > 3 and  #big_numbers > 5 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars) or (#function_1line > 3 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars) or (#big_numbers > 5 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars)) // 5 of them
	((#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2) or (#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  $encode_bug2 and  $non_printable_chars) or (#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  #char_code_at > 2 and  $non_printable_chars) or (#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars) or (#loops_count > 5 and  #function_1line > 3 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars) or (#loops_count > 5 and  #big_numbers > 5 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars) or (#function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars)) //6 of them
	//((#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  $encode_bug1) or (#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  $encode_bug2) or (#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  #char_code_at > 2) or (#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5 and  $non_printable_chars) or (#loops_count > 5 and  #function_1line > 3 and  $encode_bug1 and  $encode_bug2) or (#loops_count > 5 and  #function_1line > 3 and  $encode_bug1 and  #char_code_at > 2) or (#loops_count > 5 and  #function_1line > 3 and  $encode_bug1 and  $non_printable_chars) or (#loops_count > 5 and  #function_1line > 3 and  $encode_bug2 and  #char_code_at > 2) or (#loops_count > 5 and  #function_1line > 3 and  $encode_bug2 and  $non_printable_chars) or (#loops_count > 5 and  #function_1line > 3 and  #char_code_at > 2 and  $non_printable_chars) or (#loops_count > 5 and  #big_numbers > 5 and  $encode_bug1 and  $encode_bug2) or (#loops_count > 5 and  #big_numbers > 5 and  $encode_bug1 and  #char_code_at > 2) or (#loops_count > 5 and  #big_numbers > 5 and  $encode_bug1 and  $non_printable_chars) or (#loops_count > 5 and  #big_numbers > 5 and  $encode_bug2 and  #char_code_at > 2) or (#loops_count > 5 and  #big_numbers > 5 and  $encode_bug2 and  $non_printable_chars) or (#loops_count > 5 and  #big_numbers > 5 and  #char_code_at > 2 and  $non_printable_chars) or (#loops_count > 5 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2) or (#loops_count > 5 and  $encode_bug1 and  $encode_bug2 and  $non_printable_chars) or (#loops_count > 5 and  $encode_bug1 and  #char_code_at > 2 and  $non_printable_chars) or (#loops_count > 5 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars) or (#function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  $encode_bug2) or (#function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  #char_code_at > 2) or (#function_1line > 3 and  #big_numbers > 5 and  $encode_bug1 and  $non_printable_chars) or (#function_1line > 3 and  #big_numbers > 5 and  $encode_bug2 and  #char_code_at > 2) or (#function_1line > 3 and  #big_numbers > 5 and  $encode_bug2 and  $non_printable_chars) or (#function_1line > 3 and  #big_numbers > 5 and  #char_code_at > 2 and  $non_printable_chars) or (#function_1line > 3 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2) or (#function_1line > 3 and  $encode_bug1 and  $encode_bug2 and  $non_printable_chars) or (#function_1line > 3 and  $encode_bug1 and  #char_code_at > 2 and  $non_printable_chars) or (#function_1line > 3 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars) or (#big_numbers > 5 and  $encode_bug1 and  $encode_bug2 and  #char_code_at > 2) or (#big_numbers > 5 and  $encode_bug1 and  $encode_bug2 and  $non_printable_chars) or (#big_numbers > 5 and  $encode_bug1 and  #char_code_at > 2 and  $non_printable_chars) or (#big_numbers > 5 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars) or ($encode_bug1 and  $encode_bug2 and  #char_code_at > 2 and  $non_printable_chars)) //4 of them
        //((#loops_count > 5 and  #function_1line > 3 and  #big_numbers > 5) or (#loops_count > 5 and  #function_1line > 3 and  $encode_bug1) or (#loops_count > 5 and  #function_1line > 3 and  $encode_bug2) or (#loops_count > 5 and  #function_1line > 3 and  #char_code_at > 2) or (#loops_count > 5 and  #function_1line > 3 and  $non_printable_chars) or (#loops_count > 5 and  #big_numbers > 5 and  $encode_bug1) or (#loops_count > 5 and  #big_numbers > 5 and  $encode_bug2) or (#loops_count > 5 and  #big_numbers > 5 and  #char_code_at > 2) or (#loops_count > 5 and  #big_numbers > 5 and  $non_printable_chars) or (#loops_count > 5 and  $encode_bug1 and  $encode_bug2) or (#loops_count > 5 and  $encode_bug1 and  #char_code_at > 2) or (#loops_count > 5 and  $encode_bug1 and  $non_printable_chars) or (#loops_count > 5 and  $encode_bug2 and  #char_code_at > 2) or (#loops_count > 5 and  $encode_bug2 and  $non_printable_chars) or (#loops_count > 5 and  #char_code_at > 2 and  $non_printable_chars) or (#function_1line > 3 and  #big_numbers > 5 and  $encode_bug1) or (#function_1line > 3 and  #big_numbers > 5 and  $encode_bug2) or (#function_1line > 3 and  #big_numbers > 5 and  #char_code_at > 2) or (#function_1line > 3 and  #big_numbers > 5 and  $non_printable_chars) or (#function_1line > 3 and  $encode_bug1 and  $encode_bug2) or (#function_1line > 3 and  $encode_bug1 and  #char_code_at > 2) or (#function_1line > 3 and  $encode_bug1 and  $non_printable_chars) or (#function_1line > 3 and  $encode_bug2 and  #char_code_at > 2) or (#function_1line > 3 and  $encode_bug2 and  $non_printable_chars) or (#function_1line > 3 and  #char_code_at > 2 and  $non_printable_chars) or (#big_numbers > 5 and  $encode_bug1 and  $encode_bug2) or (#big_numbers > 5 and  $encode_bug1 and  #char_code_at > 2) or (#big_numbers > 5 and  $encode_bug1 and  $non_printable_chars) or (#big_numbers > 5 and  $encode_bug2 and  #char_code_at > 2) or (#big_numbers > 5 and  $encode_bug2 and  $non_printable_chars) or (#big_numbers > 5 and  #char_code_at > 2 and  $non_printable_chars) or ($encode_bug1 and  $encode_bug2 and  #char_code_at > 2) or ($encode_bug1 and  $encode_bug2 and  $non_printable_chars) or ($encode_bug1 and  #char_code_at > 2 and  $non_printable_chars) or ($encode_bug2 and  #char_code_at > 2 and  $non_printable_chars)) //3 of them
}

rule EK_plugins
{
	strings:

		$java = "java" nocase
		$flash = "flash" nocase
		$quicktime = "quicktime" nocase
		$pdf = "pdf" nocase
		$realplayer = "realplayer" nocase
		$activex = "activex" nocase
		$SWCtl = "SWCtl" nocase
		$WMPlayer = "WMPlayer" nocase

		$plugins = "navigator.plugins"

		$version = "version" nocase

	condition:

		7 of ($*)
}

rule EK_bit_operators
{
	strings:

		$bit_operator1 = "^"
		$bit_operator2 = "~"
		$bit_operator3 = ">>"
		$bit_operator4 = "<<"
		$bit_operator5 = ">>>"

	condition:

		(3 of ($*) and #bit_operator3 > 10) or (3 of ($*) and #bit_operator4 > 10) or (3 of ($*) and #bit_operator5 > 10) or (#bit_operator1 + #bit_operator2 + #bit_operator3 + #bit_operator4 + #bit_operator5 > 15)
}

/*rule EK_injections
{
	strings:

		$create_element = "createElement("

		$type_js = "text/javascript" nocase

		$iframe = "iframe" nocase

		$hidden = "visibility:hidden"

		$file_js = /(").*(\.js")/

	condition:

		4 of ($*)
}*/
