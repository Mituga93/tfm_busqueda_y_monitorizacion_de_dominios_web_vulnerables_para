
from ..Communication import ProtocolParser
from ..Communication import ProtocolModels
from ..Communication import SecureSocket
from .. import Log
from . import YarasHandler
try:
    import selectors
except ImportError:
    import selectors34 as selectors
import datetime
import time
import re


class AnalyzerAgent:

    def __init__(self, ip, port, key):

        Log.business_layer("INFO", "Creating new AnalyzerAgent class with: (" + ip + ", " + str(port) + ", " + key + ")")

        self.KEY = key

        #self.MANAGEMENT_SOCKET = SecureSocket.AgentSecureSocket(ip, port)
        self.WORKER_SOCKETS = []

        self.MANAGEMENT_PROTOCOL_PARSER = ProtocolParser.AnalyzerProtocolParser(key)
        self.WORKER_PROTOCOL_PARSER = []

        self.SHOULD_STOP = False
        self.STOPPED = False

        self.CONTENT = ""

        self.YARAS_HANDLER = YarasHandler.YarasHandler()

        self.add_worker_socket(SecureSocket.AgentSecureSocket(ip, port))

    def start_management(self):

        Log.business_layer("INFO", "Running management on AnalyzerAgent class with: " + self.KEY)

        while True:
            self.management_event_received()

    def start_workers(self):
        
        Log.business_layer("INFO", "Running worker(s) on AnalyzerAgent class with: " + self.KEY)

        while True:
            self.worker_event_received(self.WORKER_SOCKETS[0], 0)

    def add_worker_socket(self, socket):
        
        Log.business_layer("INFO", "Adding new worker socket on AnalyzerAgent class with: " + self.KEY)

        self.WORKER_SOCKETS.append(socket)

        self.WORKER_PROTOCOL_PARSER.append(ProtocolParser.AnalyzerProtocolParser(self.KEY))

        # self.SELECTOR.register(socket, selectors.EVENT_READ, (len(self.WORKER_SOCKETS) - 1, socket))

    def management_event_received(self):

        Log.business_layer("INFO", "Receiving messages by management socket on AnalyzerAgent with " + self.KEY)

        socket = self.MANAGEMENT_SOCKET

        msg = socket.rcv_all()

        object_received = self.MANAGEMENT_PROTOCOL_PARSER.parse_rcved_msg(msg)

        if object_received.MSG_TYPE == "HELLO":
            self.hello(socket, -1)
        elif object_received.MSG_TYPE == "RESUME":
            self.resume()
        elif object_received.MSG_TYPE == "STOP":
            self.stop()
        else:
            Log.business_layer("WARNING", "Ignored unidentified message received by management socket on AnalyzerAgent with " + self.KEY + " MSG_TYPE: " + object_received.MSG_TYPE)

    def worker_event_received(self, socket, sock_id):

        Log.business_layer("INFO", "Receiving messages by worker socket on AnalyzerAgent with " + self.KEY + " and ID: " + str(sock_id))

        msg = socket.rcv_all()

        object_received = self.WORKER_PROTOCOL_PARSER[sock_id].parse_rcved_msg(msg)

        if object_received is None:
            Log.business_layer("WARNING", "Ignored message (inherited) received by worker socket on AnalyzerAgent with " + self.KEY + "ID: " + str(sock_id))
        elif object_received.MSG_TYPE == "HELLO":
            self.hello(socket, sock_id)
        elif self.STOPPED:
            Log.business_layer("INFO", "Worker is stopped by server on AnalyzerAgent with " + self.KEY + " and ID: " + str(sock_id))
            time.sleep(10)
        elif self.SHOULD_STOP and not self.STOPPED:
            Log.business_layer("INFO", "Worker is stopped by server on AnalyzerAgent with " + self.KEY + " and ID: " + str(sock_id))
            self.STOPPED = True
            self.send_stop()
        elif object_received.MSG_TYPE == "GET CONTENT":
            self.get_content_answer(object_received, socket, sock_id)
        elif object_received.MSG_TYPE == "SET CONTENT":
            self.set_content_answer(socket, sock_id)
        else:
            Log.business_layer("WARNING", "Ignored unidentified message received by worker socket on AnalyzerAgent with " + self.KEY + "ID: " + str(sock_id) + " MSG_TYPE: " + object_received.MSG_TYPE)

    def hello(self, socket, sock_id):

        Log.business_layer("INFO", "Handling HELLO on AnalyzerAgent with " + self.KEY + " and ID: " + str(sock_id))

        if sock_id < 0:
            socket_type = "MANAGEMENT"
            protocol_parser = self.MANAGEMENT_PROTOCOL_PARSER
        else:
            socket_type = "WORKER"
            protocol_parser = self.WORKER_PROTOCOL_PARSER[sock_id]

        socket.send_all(protocol_parser.construct_to_send_msg(ProtocolModels.HelloAnswer("ANALYZER", self.KEY, socket_type)))

        time.sleep(5)

        self.send_get_content(socket, sock_id)

    def resume(self):

        Log.business_layer("INFO", "Handling RESUME on AnalyzerAgent with " + self.KEY)

        self.SHOULD_STOP = False
        self.send_resume()

    def stop(self):

        Log.business_layer("INFO", "Handling STOP on AnalyzerAgent with " + self.KEY)

        self.SHOULD_STOP = True

    def send_get_content(self, socket, sock_id):

        socket.send_all(self.WORKER_PROTOCOL_PARSER[sock_id].construct_to_send_msg(ProtocolModels.GetContentMessage("", self.KEY)))

    def get_content_answer(self, object_received, socket, sock_id):

        Log.business_layer("INFO", "Handling GET CONTENT answer on AnalyzerAgent with " + self.KEY)

        yaras_test_result = self.YARAS_HANDLER.match(object_received.CONTENT)

        self.send_set_content(socket, sock_id, object_received.HASH, "YARAS", yaras_test_result)

    def set_content_answer(self, socket, sock_id):

        Log.business_layer("INFO", "Handling SET CONTENT answer on AnalyzerAgent with " + self.KEY)

        self.send_get_content(socket, sock_id)

    def send_set_content(self, socket, sock_id, hash_, test, result):

        Log.business_layer("INFO", "Sending SET CONTENT message on AnalyzerAgent with " + self.KEY + " with: (" + result + ")")

        if result == "":
            c = "0"
        else:
            c = str(len(result.split("|")))

        msg = self.WORKER_PROTOCOL_PARSER[sock_id].construct_to_send_msg(ProtocolModels.SetContentMessage(self.KEY, hash_, test, "", c, result))

        socket.send_all(msg)

    def send_stop(self):

        Log.business_layer("INFO", "Sending STOP answer on AnalyzerAgent with " + self.KEY)

        self.MANAGEMENT_SOCKET.send_all(self.MANAGEMENT_PROTOCOL_PARSER.construct_to_send_msg(ProtocolModels.StopAnswer()))
        self.STOPPED = True

    def send_resume(self):

        Log.business_layer("INFO", "Sending RESUME answer on AnalyzerAgent with " + self.KEY)

        self.MANAGEMENT_SOCKET.send_all(self.MANAGEMENT_PROTOCOL_PARSER.construct_to_send_msg(ProtocolModels.ResumeAnswer()))
        self.STOPPED = False

