En este directorio se almacenará todo el código relacionado con el proyecto.

Se estructurará de la siguiente manera:

 - Controller_server: Almacenará el código relativo al servidor que controlará el funcionamiento de todo el sistema, interactuará con los diferentes clientes/agentes y se encargará del correcto almacenamiento de la información generada.

 - Indexer_client: Almacenará el código del cliente que permitirá comunicarse con el Controller_Server para realizar acciones sobre el módulo Indexer. Relacionadas con las tablas Indexer_CVE, Indexer_Keywords, Indexer_vulnerable_products e Indexer_hosts.

 - Crawler_agent: Almacenará el código del agente que recorrerá internet, descargará contenido, comprobará cambios y en caso de ser necesario lo almacenará para el futuro análisis. Todo este proceso estará en constante comunicación con el Controller_Server para la correcta sincronización entre los diferentes agentes.

 - Analyzer_agent: Almacenará el código del agente que analizará los contenidos descargados y marcados por los diferentes Crawler_agents. Todo este proceso estará en constante comunicación con el Controller_server para la correcta sincronización con los diferentes agentes.


A su vez, se tratará de que todos los módulos sigan un modelo de 3 capas, organizándose así en las siguientes subcarpetas:

 - Data: Será el código relativo al almacenamiento en la base de datos o en el sistema de ficheros.

 - Communication: Será el código encargado de tratar todas las comunicaciones entre el Controller_server y los clientes/agentes

 - Negocio: Será el código encargado de realizar todos los trabajos necesarios como el procesamiento de la información. Será una capa intermedia entre la anterior y la siguiente.

 - Vista: Será el código encargado de comunicarse con el usuario que ejecuta el programa
