\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducci'on}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Introducci'on}{4}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}?`Qu'e son los Exploit Kit?}{4}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Historia de los Exploit Kit}{5}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Motivaci'on}{8}{section.1.2}
\contentsline {section}{\numberline {1.3}Objetivos y alcance}{9}{section.1.3}
\contentsline {chapter}{\numberline {2}Antecedentes}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Soluciones actuales}{11}{section.2.1}
\contentsline {chapter}{\numberline {3}Planificaci'on temporal}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Diagrama de Gantt}{14}{section.3.1}
\contentsline {section}{\numberline {3.2}Contratiempos y retrasos}{15}{section.3.2}
\contentsline {section}{\numberline {3.3}Mejoras pendientes}{15}{section.3.3}
\contentsline {chapter}{\numberline {4}An'alisis del sistema.}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}Actores}{17}{section.4.1}
\contentsline {section}{\numberline {4.2}Flujo del sistema}{18}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}ControllerServer}{18}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}CrawlerAgents}{19}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}AnalyzerAgents}{20}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Base de datos}{21}{section.4.3}
\contentsline {section}{\numberline {4.4}Protocolo definido}{22}{section.4.4}
\contentsline {chapter}{\numberline {5}Dise\~no: arquitectura y detalle.}{24}{chapter.5}
\contentsline {section}{\numberline {5.1}Requisitos del sistema}{24}{section.5.1}
\contentsline {section}{\numberline {5.2}Arquitectura}{24}{section.5.2}
\contentsline {section}{\numberline {5.3}Detalles de implementaci'on}{27}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}ControllerSever}{27}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}CrawlerAgents}{28}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}AnalyzerAgents}{30}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Indexer}{31}{subsection.5.3.4}
\contentsline {chapter}{\numberline {6}Pruebas}{33}{chapter.6}
\contentsline {section}{\numberline {6.1}Pruebas Indexer}{33}{section.6.1}
\contentsline {section}{\numberline {6.2}Pruebas CrawlerAgent}{34}{section.6.2}
\contentsline {section}{\numberline {6.3}Pruebas AnalyzerAgent}{36}{section.6.3}
\contentsline {section}{\numberline {6.4}Pruebas ControllerServer}{37}{section.6.4}
\contentsline {section}{\numberline {6.5}Pruebas de integraci'on}{38}{section.6.5}
\contentsline {chapter}{\numberline {7}Conclusiones}{39}{chapter.7}
\contentsline {section}{\numberline {7.1}Indexer}{39}{section.7.1}
\contentsline {section}{\numberline {7.2}Crawler}{40}{section.7.2}
\contentsline {section}{\numberline {7.3}Analyzer}{41}{section.7.3}
\contentsline {section}{\numberline {7.4}Generales}{42}{section.7.4}
\contentsline {chapter}{\numberline {A}Repositorio}{45}{appendix.Alph1}
\contentsline {chapter}{\numberline {B}User agents recopilados}{46}{appendix.Alph2}
\contentsline {chapter}{\numberline {C}Afectaci'on seg'un las regiones geogr'aficas}{48}{appendix.Alph3}
