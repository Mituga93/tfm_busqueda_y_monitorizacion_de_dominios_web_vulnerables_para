BÚSQUEDA Y MONITORIZACIÓN DE DOMINIOS WEB VULNERABLES PARA EL INDEXADO Y ESTUDIO DE EXPLOIT KITS

El proyecto pretenderá atacar a estas herramientas precisamente en el proceso de infección. Para ser realmente efectivos y que sus códigos maliciosos se lleguen a ejecutar en multitud de victimas, necesitan inyectarse en web legítimas con un tráfico relativamente alto.
La idea es conseguir destectar, lo antes posible, cuándo una página legitima cualquiera ha sido modificada para incluir alguna redirección o recurso de una página maliciosa. La estructura simplificada cubriria los siguientes procesos:
 

 1. Indexar dominios web potencialmente vulnerables.
 2. Analizarlos en busca de exploit kits previamente activos.
 3. Monitorizar continuamente los cambios realizados en las páginas de dichos dominios.
 4. Analizar los cambios producidos en busca de nuevos exploit kits.

El sistema proveerá información en tiempo real sobre los tipos y las campañas de exploit kits que estén activas en cada momento permitiendo asi poder limitar sus efectos adversos mediante, por ejemplo, la generación de indicadores de compromiso. Además el sistema almacerá multitud de instancias concretas de exploit kits, con esta muestra que será lo suficientemente grande se podrá investigar con una mayor precisio las diferentes estructuras  y similitudes que se encuentran entre ellos, con el objetivo de mejorar las técnicas de detección generales de este tipo de amenazas.
